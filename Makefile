all:main

CFLAGS=

SOURCEC=triangle.c main.c
SOURCEH=Functions2LM.h 
OBJSC=triangle.o main.o 

include ${PETSC_DIR}/conf/variables
include ${PETSC_DIR}/conf/rules

main: $(OBJSC) chkopts
	${CLINKER} -o main $(OBJSC) ${PETSC_LIB} 
	${RM} $(OBJSC)
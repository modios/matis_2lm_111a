/*
 * main.c
 *
 *  Created on: 16 Jan 2014
 *      Author: anastasisos
 */
//mpirun -n 1 ./main for basic tests.

static char help[]="MATIS_2LM solver 1.0.1c/Look at Readme.md for more details and examples";

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <math.h>
#include <assert.h>
#include <time.h>
#include "triangle.h"
#include "petsc.h"
#include "Functions2LM.h"
#include "Data.h"

#define _abs(x)    (x)<0?-(x):x
#define _min(x,y) (x)<(y)?(x):(y)
#define _max(x,y) (x)>(y)?(x):(y)
//#define PI 3.14159265358979
#ifndef PI
#define PI 3.141592653589793238462643383279502884197169399375105820974944592308
#endif


/*void SeqStiff_Mat(int n,double *Avalues,int* grows,int *gcol,int sizeA,\
		double ***Af){
	double** A;
	int i,j;
	A = (double**) malloc(n*sizeof(double*));
	for (i = 0; i <n ; i++)
		A[i] = (double*) malloc(n*sizeof(double));
	double *f=malloc(n*sizeof(double));
	for(i=0;i<sizeA;i++){
		A[grows[i]][gcol[i]]=A[grows[i]][gcol[i]]+Avalues[i];
	}

	*Af=A;
}*/


int compare_matrix_int1D(int* A,int *B,int rowsizeA,int rowsizeB,int colsizeA,int colsizeB){
	unsigned int i,j;
	if(( rowsizeA!= rowsizeB) || (colsizeA!=colsizeB) ){
		fprintf(stderr, "Error: Matrices have different sizes!! .\n");
		return 1;
	}
	for(i=0;i<rowsizeA;i++){
		for(j=0;j<colsizeA;j++){
			if(A[i*colsizeA+j]!=B[i*colsizeB+j]){
				fprintf(stderr, "Error: Different Matrices!! .\n");
				return 1;
			}
		}
	}

	return 0;
}

int compare_matrix_double1D(double* A,double *B,int rowsizeA,int rowsizeB,int colsizeA,int colsizeB){
	unsigned int i,j;
	if(( rowsizeA!= rowsizeB) || (colsizeA!=colsizeB) ){
		fprintf(stderr, "Error: Matrices have different sizes!! .\n");
		return 1;
	}
	for(i=0;i<rowsizeA;i++){
		for(j=0;j<colsizeA;j++){
			if(A[i*colsizeA+j]!=B[i*colsizeB+j]){
				fprintf(stderr, "Error: Different Matrices!! .\n");
				return 1;
			}
		}
	}

	return 0;
}



int unitest_unisquare(int rank,int sub,int data,int NE){
	int T0[] = {0,1,2,
			0,2,3};
	double V0[] = {0,0,
			1,0,
			1,1,
			0,1};
	int m=2,n=4;
	int *T1 = &T0[0],ref=1,i;
	double *V1 = &V0[0];
	for(i=0;i<sub;i++){
		uniform(&T1,&m,&V1,&n,&ref);
	}
	int sizeA;
	int *grows,*gcols,*gfr,sizef;
	double *gfval;
	double* AMat,**A;
	//printit(T1,m,V1,n,rank);
	Globall_stiffness_Mat(&AMat,&sizeA,\
			&grows,&gcols,T1,m,V1,n,\
			&gfval,&gfr,&sizef);
	SeqStiff_Mat(n,AMat,grows,gcols,sizeA,&A);
	printf("Stiffness matrix %dx%d on uniform square\n",n,n);
	int j,flag=1;
	double tol=1e-5;
	for(i=0;i<n;i++){
		for(j=0;j<n;j++){
			if(data==0){
				if(_abs(A[i][j]-A0[i][j])>tol){
					fprintf(stderr, "Error: in %dx%d stiffnes Matrix .\n",n,n);
					flag=0;
				}
			}
			if(data==1){
				//printf("%f %f\n ",A[i][j],A1[i][j]);
				if(_abs(A[i][j]-A1[i][j])>tol){
					fprintf(stderr, "Error: in %dx%d stiffnes Matrix .\n",n,n);
					flag=0;
				}
			}
		}
	}
	if(flag) printf("Stiffness matrix %dx%d on uniform square :OK\n ",n,n);
	flag=1;

	double* fele=malloc(sizeof(double)*n);
	for(i=0;i<n;i++) fele[i]=0.0;
	for(i=0;i<sizef;i++){
		fele[gfr[i]]=fele[gfr[i]]+gfval[i];
	}
	//for(i=0;i<n;i++) printf("%f %f\n",fele[i],b1[i]);
	if(data==1){ for(i=0;i<n;i++){if(!(_abs((fele[i]-b1[i]))<tol)) fprintf(stderr, "Error: in %d on rhs.\n",n,n); }}
	printf("Check boundary ");
	int *boundary,*edgelist,sizelist;

	Mesh_info_f(T1,n,m,&boundary,&edgelist,&sizelist);
	printf("boundary:\n");
	for(i=0;i<n;i++){
		if(boundary[i]==1){
			//printf("%d\n",i);
		}
	}
	printf("sizelist");
	for(i=0;i<sizelist;i++){
		//printf("%d %d\n",edgelist[2*i],edgelist[2*i+1]);
	}
	flag=1;
	int  *globalofset;
	printf("Check offset %dx%d on uniform square\n",n,n);
	offset(T1, m,V1,n, &globalofset,
			boundary,NE,edgelist,sizelist);
	for(i=0;i<m;i++){
		if(data==0) if(_abs(globalofset[i]-Goff0[i])!=0){
			fprintf(stderr, "Error: in %dx%d Goffset .\n",n,n);
			flag=0;
		}
		if(data==1) if(_abs(globalofset[i]-Goff1[i])!=0){
			fprintf(stderr, "Error: in %dx%d Goffset .\n",n,n);
			flag=0;
		}
	}
	if(flag==1) printf("Check offset %dx%d on uniform square: OK\n",n,n);

	int *cumsum;
	Cumsumfunc(globalofset,&cumsum,m);
	for(i=0;i<m;i++){
		//printf("%d\n",cumsum[i]);
	}
	printf("Check cumsum %dx%d on uniform square\n",n,n);
	flag=1;
	for(i=0;i<m;i++){
		if(data==0) if(_abs(cumsum[i]-cumsum0[i])!=0){
			fprintf(stderr, "Error: in %dx%d cumsumt.\n",n,n);
			flag=0;
		}
		if(data==1) if(_abs(cumsum[i]-cumsum1[i])!=0){
					fprintf(stderr, "Error: in %dx%d cumsumt.\n",n,n);
					flag=0;
		}
	}
	if(flag){ printf("Cumsum %dx%d : OK\n ",n,n);}

	return 0;


}

int unitest_circle(int rank,int NE){
	int *T1;
	double *V1;
	int m,n;
	circle_info info;

	info.center_x=0.0;
	info.center_y=0.0;
	//info.maximum_triangle_area=0.2;
	info.maximum_triangle_area=.1; //this is the value to check
	info.radius=2; //this is the value to check
	//info.radius=0.5;
	info.min_angle=30;
	info.number_of_points=22; //this is the value to check
	//info.number_of_points=11;
	circular_mesh(&T1,&m,&V1,&n,info);
	//printit(T1,m,V1,n,rank);
	int i;

	int sizeA;
	int *grows,*gcols,*gfr,sizef;
	double *gfval;
	double* AMat,**A;
	//printit(T1,m,V1,n,rank);
	Globall_stiffness_Mat(&AMat,&sizeA,\
			&grows,&gcols,T1,m,V1,n,\
			&gfval,&gfr,&sizef);
	SeqStiff_Mat(n,AMat,grows,gcols,sizeA,&A);
	printf("Stiffness matrix %dx%d on non structured circle\n",n,n);
	int j,flag=1;
	double tol=1e-5;
	double* fele=malloc(sizeof(double)*n);
	for(i=0;i<n;i++) fele[i]=0.0;
	for(i=0;i<sizef;i++){
		fele[gfr[i]]=fele[gfr[i]]+gfval[i];
	}
	//for(i=0;i<n;i++) printf("%f %f\n",fele[i],b2[i]);
	for(i=0;i<n;i++){
		for(j=0;j<n;j++){
			if(!(_abs(((A[i][j]-A2[i][j]))<tol))){
				//printf("%f %f %f\n ",A[i][j],A2[i][j],_abs((A[i][j]-A2[i][j])));
				fprintf(stderr, "Error: in %dx%d stiffnes Matrix .\n",n,n);
				flag=0;
				break;
			}
		}
	}

	for(i=0;i<n;i++){if(!(_abs((fele[i]-b2[i]))<tol)) fprintf(stderr, "Error: in %d on rhs.\n",n,n); }
	if(flag) printf("Stiffness matrix %dx%d on non structured circle :OK\n ",n,n);
	flag=1;

	printf("Check boundary ");
	int *boundary,*edgelist,sizelist;

	Mesh_info_f(T1,n,m,&boundary,&edgelist,&sizelist);
	printf("boundary:\n");
	int cout=0;
	flag=0;
	for(i=0;i<n;i++){
		if(boundary[i]==1){
			if((i-boundary_cir[cout])!=0){
				fprintf(stderr,"problem with boundary circle %dx%d on rhs.\n",n,n);
				flag=1;
			}
			cout++;
		}
	}
	if(flag)printf("boundary OK");
	printf("sizelist");
	for(i=0;i<sizelist;i++){
		//printf("%d %d\n",edgelist[2*i],edgelist[2*i+1]);
	}
	flag=1;
	int  *globalofset;
	printf("Check offset %dx%d on circle\n",n,n);
	offset(T1, m,V1,n, &globalofset,
			boundary,NE,edgelist,sizelist);
	for(i=0;i<m;i++){
		if(_abs(globalofset[i]-Goff3[i])!=0){
			fprintf(stderr, "Error: in %dx%d Goffset .\n",n,n);
			flag=0;
		}
	}
	if(flag==1) printf("Check offset %dx%d on circle OK\n",n,n);

	int *cumsum;
	Cumsumfunc(globalofset,&cumsum,m);
	for(i=0;i<m;i++){
	flag=1;
		for(i=0;i<m;i++){
		     if(_abs(cumsum[i]-cumsum2[i])!=0){
				fprintf(stderr, "Error: in %dx%d cumsum.\n",n,n);
				flag=0;
	        }
		}
	}
	if(flag==1) printf("Check cumsum %dx%d on circle OK\n",n,n);
	return 0;
}


int unitest_parallel(int rank,int sub,int NE){
	int T0[] = {0,1,2,
				0,2,3};
		double V0[] = {0,0,
				1,0,
				1,1,
				0,1};
		int m=2,n=4;
		int *T1 = &T0[0],ref=1,i;
		double *V1 = &V0[0];
		for(i=0;i<sub;i++){
			uniform(&T1,&m,&V1,&n,&ref);
		}

		int * Tseq,*Tseq2,m1,n1;
		double *Vseq;
		int proc=rank,neibours[12],size_of_neib,*master_offset;;
		int *cumsum;

		//Sequenctial_Triangle(&T1[3 * proc], V1, n, NE, &Tseq, &m1, &Vseq, &n1);//Local mesh
		//printit(Tseq,m1,Vseq,n1,rank);

	return 0;
}

void unit_print(int subdivisions,int NEdges,int size){

	int i,sizeALoc,count=0,proc;
		double *PMAT_Stiff,* gfvalues;
		int sizePMAT,* Global_columns,* Global_rows,* gfrows,sizef;
		int sizebound,*GlobalOffset,sizeV;
		int* renumber;
		/**PETSC VARIABLES**/
		Mat  A;
		Vec  f;
		PetscErrorCode ierr;
		PetscScalar  ValueA;
		PetscInt row,column;

      for(i=0;i<size;i++){
		Proc_Stiffnes_Mat(i,NEdges,subdivisions,\
				&PMAT_Stiff,&sizePMAT,&Global_columns,&Global_rows,
				&gfvalues,&gfrows,&sizef,&GlobalOffset,&sizebound,&sizeV,&renumber);

      }
}

int main(int argc, char **argv){

  PetscMPIInt rank,size_world;
  int NE=100;

  PetscInitialize(&argc,&argv,(char *)0,0);
  MPI_Comm_rank(PETSC_COMM_WORLD,&rank);
  MPI_Comm_size(PETSC_COMM_WORLD,&size_world);
  if(rank==0){
  unitest_unisquare(rank,1,0,3);
  unitest_unisquare(rank,2,1,3);
  unitest_circle(rank,3);
  }

   //if(rank==size_world-1) unitest_parallel(rank,1,3);*/
   //if(rank==0) unit_print(1,NE,size_world);
   PetscFinalize();

 }

######### MATIS_2LM 1.1.1a Solver ######### 

You can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

MATIS_2LM Solver is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

######### How to use the code in Three Steps######### 

-->First Step

	In Functions2LM.h file, set your initial geometry in the
	Proc_Stiffness_Mat function.

*! IMPORTANT: The number of processor should be N=n*4^r
where n is the number of initial triangles, r is the number of times
that you will refine the initial mesh (T0,V0).

*******************Example 1*****************************************

	int T0[] = { 0, 1, 2, 0, 2, 3 }; //Define T0: the triangles

	double V0[] = { 0, 0, 1, 0, 1, 1, 0, 1 }; //Define V0: the coordinates

	int array20[] = { 0, 1, 2, 3 }; // set which vertices are on your 		   initial natural boundary
	
	int m = 2, n = 4; // set n: the number vertices on the initial boundary
					 // set m: the number of triangle
	int second = 4;  // number of initial vertices 

*******************Example 2*****************************************

	int T0[] = {1, 0, 4,
				2, 1, 4,
				3, 2, 4,
				0, 3, 4};
	
	double V0[] = {0,0,  //Vertices
					1,0,
					1,1,
					0,1,
					0.5,0.5};
	int array20[]={0,1,2,3}; //informaition for the natural boundary.


	int second=4; //number of  vertice on the boundary
	int m = 4, n = 5; //m:number of initail triangles / n:number of initial 		vertices */



-->Second Step
	In main.c file,
	
	Choose the number of vertices per edge.
    int NEdges=pow(2,2)-1; //Number of vertices

    Choose the number of times you want to refine the initial mesh (T0,V0).
	int subdivisions=2; //Number of subdivisions of the inietal mesh.

	You can choose your Robin parammeter by changing
	Robin_par=sqrt(1.0/(NEdges)).

	Just for presention issues I have icluded some printing from proc 0.
	You can erase all printing.

	Finally, the function RecoverSolution returns the solution 
	for each processor a sequential vector.
	In the program I choose only one proc. to print the solution
	but you can remove the if condition to make all procs to print their
	solution.

-->Third step. 

	$make,
	$mpirun -n X ./main -n XX
	where X is the number of subdomains
	and XX is 0 if you want to use symmetric lagrange multiplier,
	and XX is 1 if you want to use the non symmetric version.
	


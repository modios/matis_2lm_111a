/*### MATIS_2LM 1.1.1a Solver #########
You can redistribute it and/or modify it under the terms of the GNU General Public License as published
 by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.
MATIS_2LM Solver is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without 
even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.You should have received a copy of the 
GNU General Public License along with this program. If not, see http://www.gnu.org/licenses/.
*/

/*
 * Functions2LM.h
 *
 *  Created on: 16 Jan 2014
 *      Authors:Anastasios Karangelis - Sebastien Loisel
 */



#define _abs(x)    (x)<0?-(x):x
#define _min(x,y) (x)<(y)?(x):(y)
#define _max(x,y) (x)>(y)?(x):(y)

//#define PI 3.14159265358979
#ifndef PI
#define PI 3.141592653589793238462643383279502884197169399375105820974944592308
#endif

/************* Linear Search ************************************
 *  Linear search or  sequential search good for search in lists
 *  with few elements
 */
int linearSearchInt(int*A, int target, int min, int max) {
	int i;
	for (i = min; i < max; i++) {
		if (A[i] == target) {
			return i;
		}
	}
	return -1;
}

int compare_doubles (const void *a, const void *b)
{
	const double *da = (const double *) a;
	const double *db = (const double *) b;

	return (*da > *db) - (*da < *db);
}

int compareints (const void * a, const void * b)
{
  return ( *(int*)a - *(int*)b );
}


int print_array_int(int* array,int size,int num){
	int i;
	printf("C%d=[\n",num);
	for(i=0;i<size;i++){
		printf("%d\n",array[i]);
	}
	printf("];");
	return 0;
}

//printint: Used to print matrices in a nice Matlab way V,T
void printit(int *T, int m, double *V, int n, int num) {
	int i;
	printf("T%d=[", num);
	for (i = 0; i < m; ++i) {
		printf("%i, %i, %i,\n", T[3 * i], T[3 * i + 1], T[3 * i + 2]);
	}
	puts("];");
	printf("V%d=[", num);
	for (i = 0; i < n; ++i) {
		printf("%f, %f,\n", V[2 * i], V[2 * i + 1]);
	}
	puts("];");
}

int counts_func(int *A, int* count, int n) {
	int i;
	for (i = 0; i < n; i++) {
		count[A[i]]++;
	}
	return 0;
}

//helps with  qsrot lexicographical order.
int lexorder2(const void* arr1, const void* arr2) {
	/* Convert back to the proper type. */
	const int* one = *(const int**) arr1;
	const int* two = *(const int**) arr2;
	unsigned int i;
	for (i = 0; i < 2; i++) {
		if (one[i] < two[i]) return -1;
		if (one[i] > two[i]) return +1;
	}
	return 0;
}
void print_Mat(int n,double *Avalues,int* grows,int *gcol,int sizeA,\
		double* fval,int* frows,int sizef){
	double** A;
	unsigned int i,j;
	A = (double**) malloc(n*sizeof(double*));
	for (i = 0; i <n ; i++)
	   A[i] = (double*) malloc(n*sizeof(double));
	double *f=malloc(n*sizeof(double));
	for(i=0;i<sizeA;i++){
	A[grows[i]][gcol[i]]=A[grows[i]][gcol[i]]+Avalues[i];
	}


	for(i=0;i<n;i++){
		for(j=0;j<n;j++){
		printf("%f,",A[i][j]);
	}
    printf("\n");
	}

	for(i=0;i<sizef;i++){
		f[frows[i]]=f[frows[i]]+fval[i];
	}

	for(i=0;i<n;i++){
			printf("%f\n",f[i]);
	}
}


//**fix orientation of trianles in order to have positive determinant**//
void fix_triangle_orientation(int *T, int m, double *V, int n){
	unsigned int i;
	int temp1;
	for (i=0;i<m;i++){
		if(((V[2*T[3*i+1]]-V[2*T[3*i]])*(V[2*T[3*i+2]+1]-V[2*T[3*i]+1])\
				-(V[2*T[3*i+1]+1]-V[2*T[3*i]+1])*(V[2*T[3*i+2]]-V[2*T[3*i]]))<0){
			temp1=T[3*i+1];
			T[3*i+1]=T[3*i+2];
			T[3*i+2]=temp1;
		}
	}
}

void SeqStiff_Mat(int n,double *Avalues,int* grows,int *gcol,int sizeA,\
		double ***Af){
	double** A;
	int i,j;
	A = (double**) malloc(n*sizeof(double*));
	for (i = 0; i <n ; i++)
		A[i] = (double*) malloc(n*sizeof(double));
	double *f=malloc(n*sizeof(double));
	for(i=0;i<sizeA;i++){
		A[grows[i]][gcol[i]]=A[grows[i]][gcol[i]]+Avalues[i];
	}

	*Af=A;
}

/*******************************************************************************
 * 						MESH MANIPULATION 									   *
 * 						RELATED CODE										   *
 *******************************************************************************/

/*      uniform: Uniform mesh generation for P1 elements
 *
 * Inputs:
 * 		T:initial triangles
 * 		m:number of initial triangles
 * 		V:initial coordinates
 *		n:number of initial vertices
 *		ref: variable that holds the times that we have refined the mesh
 *
 * Output: The mesh after one refinement.
 *
 * example:
 *	    int T0[] = {0,1,2,
 *	    		    0,2,3};
 * 	  	double V0[] = {0,0,
 *            		   1,0,
 *            		   1,1,
 *             		   0,1};
 *     int array0[]={0,2},array20[]={0,1,2,3}
 *     int m = 2, n = 4;
 *     int *T = &T0[0],ref=1;
 *     double *V = &V0[0];
 *     subdiv(&T,&m,&V,&n,0,&ref,&type_of_triag);
 *
 */

void uniform(int **T, int *m, double **V, int *n,int *refinment) { //subdiv function
	int *T0 = *T, m0 = *m, n0 = *n, ref = *refinment;
	double *V0 = *V;
	int *E = malloc(sizeof(int) * 6 * m0); //array of [v1]-[v2] (edges) [2*3]*number of triangles
	int **I = malloc(sizeof(int *) * 6 * m0);
	int i, j, a, b;

	for (i = 0; i < m0; ++i) {
		for (j = 0; j < 3; ++j) {
			a = T0[3 * i + j];
			b = T0[3 * i + ((j + 1) % 3)];
			E[6 * i + 2 * j] = _min(a,b);
			E[6 * i + 2 * j + 1] = _max(a,b);
		}
	}
	for (i = 0; i < m0 * 3; i++)
		I[i] = &E[2 * i];
	//qsort(I, m0 * 3, sizeof(int *), lexorder);
	qsort(I, m0 * 3, sizeof(int *),lexorder2);
	int *en = malloc(sizeof(int) * 3 * m0); //number the initial edges
	en[0] = 0;
	for (i = 1; i < 3 * m0; i++) {
		en[i] = en[i - 1] + (lexorder2(&I[i - 1], &I[i]) != 0);
	}
	int n1 = en[3 * m0 - 1] + n0 + 1;
	//printf("The n1 is %d\n",n1);
	double *V1 = malloc(sizeof(double) * 2 * n1); //matrix that will store the new vertex points
	for (i = 0; i < 2 * n0; ++i)
		V1[i] = V0[i];
	int c;
	for (i = 0; i < m0 * 3; ++i) { //Creating the part of matrix V with the new vertices.
		a = I[i][0];
		b = I[i][1];
		c = en[i];
		V1[2 * n0 + 2 * c] = 0.5 * (V0[2 * a] + V0[2 * b]);
		V1[2 * n0 + 2 * c + 1] = 0.5 * (V0[2 * a + 1] + V0[2 * b + 1]);
	}
	int *T1 = malloc(sizeof(int) * 12 * m0);
	for (i = 0; i < 4 * m0; ++i)
		T1[i] = 0;
	int mt = m0;
	int *x = malloc(3 * m0 * sizeof(int));
	int k;
	for (i = 0; i < 3 * m0; i++) {
		k = (I[i] - &E[0]) / 2;
		x[k] = en[i];
	}
	//*****count*****//
	int countsize = en[3 * m0 - 1] + 1;
	int *count = malloc(sizeof(int) * (countsize));
	for (i = 0; i < countsize; i++)
		count[i] = 0;

	counts_func(en, count, 3 * m0); //how many times x[i] appears, if 2 then interface.
	//****endcount****//

	for (i = 0; i < mt; i++) {
		a = x[3 * i];
		b = x[3 * i + 1];	//
		c = x[3 * i + 2];	//

		/* Red triangles */
		T1[3 * i] = T0[3 * i];
		T1[3 * i + 1] = a + n0;
		T1[3 * i + 2] = c + n0;

		/* Green triangles */
		T1[3 * i + 3 * m0] = T0[3 * i + 1];
		T1[3 * i + 1 + 3 * m0] = a + n0;
		T1[3 * i + 2 + 3 * m0] = b + n0;

		/* black triangles */
		T1[3 * i + 6 * m0] = T0[3 * i + 2];
		T1[3 * i + 1 + 6 * m0] = b + n0;
		T1[3 * i + 2 + 6 * m0] = c + n0;
		/*blue triangles */
		T1[3 * i + 9 * m0] = a + n0;
		T1[3 * i + 1 + 9 * m0] = b + n0;
		T1[3 * i + 2 + 9 * m0] = c + n0;

	}
	/** free **/
	free(en);
	free(x);
	free(count);
	free(I);
	free(E);
	//******search end *****//
	fix_triangle_orientation(T1,4*m0,V1,n1);

	*T = T1;
	*V = V1;
	*n = n1;
	*m = 4 * m0;
	*refinment = ref + 1;
}


/*********************************************************************************
 *					ASSEBLING THE STIFFNESS MATRIX                               *
 * 							RELATED CODE										 *
 *********************************************************************************/

/*************************** The Conductivity function********************************
 * Here for example we have the constant function equal to 1
 * input:
 * 		a: x coordinate
 * 		b: y coordinate
 * output:
 * 		the values of the conductivity term
 */
double conductivity(double a, double b) {
	return 1.0;				//exp(-(a-0.1)*(a-0.1)/2-(b-0.1)*(b-0.1)/2);
}

/*************************** The RHS function********************************
 * Here for example we have the constant function equal to 1
 * input:
 * 		a: x coordinate
 * 		b: y coordinate
 * output:
 * 		the values of a function
 */
double LoadFunc(double a, double b) {
	return 1.0;
}

/***************************P1 elements Local RHS vector *******************
 * Function that return the Local RHS vector for P1 Elements
 *
 * input/output:
 *    		fvalues:where the local values of f will be stored
 * 			V: the local cordinates of the vertice
 * 			J: the determinant
 *
 */

int LocLoadVector2D(double* fvalues, double *V, double J) {

	fvalues[0] = J * LoadFunc(V[0], V[1]) / 3.0;
	fvalues[1] = J * LoadFunc(V[2], V[3]) / 3.0;
	fvalues[2] = J * LoadFunc(V[4], V[5]) / 3.0;

	return 0;
}

/***************************P1 elements Local stiffness matrix *******************
 * Function that return the Local stiffness matrix for P1 Elements
 *
 * inputs/outputs:
 * 			LocalValues:pointer to LocalValues(Here the local values will be stored)
 * 			Localrows: pointer to Localrows(Here the local rows will be stored)
 * 			Localcolumn: pointer to Loaclcolumns(Here the localcolumns will be stored)
 * 			T:  triangle
 * 			sizeT: not needed(TO DO)
 * 			V: Local vertices
 * 			sizeV: not needed(TO DO)
 * 			localfvalues: Local Load vector
 * 			frows:rows of the local Load vector
 *
 */
int Local_Stiffness_Matrix(double **LocalValues, int **Localrows,
		int **Localcolumns, int *T, int sizeT, double *V, int sizeV,
		double* localfvalues, int* frows) {
	double J;
	double Vtemp[6];
	double b1, b2, b3, c1, c2, c3;
	double abar;
	double* Mfinal = malloc(10 * sizeof(double));

	int count = 0;
	int i, j;

	int* LocalrowsFinal = malloc(sizeof(int) * 10);
	int* LocalcolumnFinal = malloc(sizeof(int) * 10);

	for (i = 0; i < 3; i++) {
		Vtemp[2 * i] = V[2 * T[i]];
		Vtemp[2 * i + 1] = V[2 * T[i] + 1];
	}

	for (i = 0; i < 3; i++) {
		for (j = 0; j < 3; j++) {
			LocalrowsFinal[count] = T[i];
			LocalcolumnFinal[count] = T[j];
			count++;
		}
	}
	b1 = Vtemp[3] - Vtemp[5];
	b2 = Vtemp[5] - Vtemp[1];
	b3 = Vtemp[1] - Vtemp[3];
	c1 = Vtemp[4] - Vtemp[2];
	c2 = Vtemp[0] - Vtemp[4];
	c3 = Vtemp[2] - Vtemp[0];

	abar = conductivity((Vtemp[0] + Vtemp[2] + Vtemp[4]) / 3.0, (Vtemp[1] + Vtemp[3] + Vtemp[5]) / 3.0); //(center of Gravity)

	J = _abs(((c3 * b2) - (c2 * b3)))/ 2.0;
	//J = ((c3 * b2) - (c2 * b3))/ 2.0;
	Mfinal[0] = abar * ((b1 * b1) + (c1 * c1)) / (4.0 * J);
	Mfinal[1] = abar * ((b1 * b2) + (c1 * c2)) / (4.0 * J);
	Mfinal[2] = abar * ((b1 * b3) + (c1 * c3)) / (4.0 * J);
	Mfinal[3] = abar * ((b2 * b1) + (c2 * c1)) / (4.0 * J);
	Mfinal[4] = abar * ((b2 * b2) + (c2 * c2)) / (4.0 * J);
	Mfinal[5] = abar * ((b2 * b3) + (c2 * c3)) / (4.0 * J);
	Mfinal[6] = abar * ((b3 * b1) + (c3 * c1)) / (4.0 * J);
	Mfinal[7] = abar * ((b3 * b2) + (c3 * c2)) / (4.0 * J);
	Mfinal[8] = abar * ((b3 * b3) + (c3 * c3)) / (4.0 * J);


	LocLoadVector2D(localfvalues, Vtemp, J);

	frows[0] = T[0];
	frows[1] = T[1];
	frows[2] = T[2];

	*LocalValues = Mfinal;
	*Localrows = LocalrowsFinal;
	*Localcolumns = LocalcolumnFinal;
	return 0;
}


/***************************P1 elements Global stiffness matrix *******************
 * Function that return the global stiffness matrix for P1 Elements
 *
 * inputs:
 * 			Global_stiffnes: pointer for the Values of Global Stiffness Matrix
 * 			sizeGlobal:pointer for the size of the Global stiffness Matrix
 * 			Global_columns: pointer for the Global columns
 * 			Global_rows: pointer for the Global rows
 * 			T: triangels
 * 			sizeT: number of Triangles
 * 			V: coordinates of the vertices
 * 			sizeV: number of coordinates
 * 			gfvalues:
 * 		   	gfrows:
 * 		   	sizef:
 *
 *
 */
int Globall_stiffness_Mat(double **Global_stiffness, int* sizeGlobal,
		int** Global_columns, int** Global_rows, int* T, int sizeT, double *V,
		int sizeV, double** gfvalues, int** gfrows, int* sizef) {
	int i, j, N = 10 * sizeT;
	int* globalrows = malloc(sizeof(int) * N);
	int* globalcolumns = malloc(sizeof(int) * N);
	int* storeglobalfrows = malloc(sizeof(int) * N);
	double* globalvalues = malloc(sizeof(double) * N);
	double* globalfvalues = malloc(sizeof(double) * N);

	double fvalues[3];
	int storelocalrows[3];
	double* localValues;
	int *localrows, *locaColumns;

	int count = 0, count1 = 0;

	for (i = 0; i < N; i++) {
		globalrows[i] = 0;
		globalcolumns[i] = 0;
		globalvalues[i] = 0;
	}

	for (j = 0; j < sizeT; j++) {
		Local_Stiffness_Matrix(&localValues, &localrows, &locaColumns,
				&T[3 * j], sizeT, V, sizeV, fvalues, storelocalrows);
		for (i = 0; i < 9; i++) {
			globalrows[count] = localrows[i];
			globalcolumns[count] = locaColumns[i];
			globalvalues[count] = localValues[i];
			count += 1;
		}
		for (i = 0; i < 3; i++) {
			storeglobalfrows[count1] = storelocalrows[i];
			globalfvalues[count1] = fvalues[i];
			count1 += 1;
		}
		free(localrows);
		free(locaColumns);
		free(localValues);
	}
	//printf("------\n");

	*gfvalues = globalfvalues;
	*gfrows = storeglobalfrows;
	*sizeGlobal = count;
	*sizef = count1;
	*Global_columns = globalcolumns;
	*Global_rows = globalrows;
	*Global_stiffness = globalvalues;

	return 0;
}


void Mesh_info_f(int *T,int n,int m,int** boundaryf,int** edge_boundary_list,int* sizef){
	PetscInt i,j;
	int a,b,nz_u,nz_a;
	Mat A;
	PetscInt *cnt=calloc(n,sizeof(PetscInt)),*rows,*cols,ncols;
	const  PetscInt *cols1;
	PetscScalar one=1.0;
	const PetscScalar *values;
	Vec Vc;
	int *boundary=calloc(n,sizeof(int));

	PetscMalloc(3*m*sizeof(PetscInt),&rows);
	PetscMalloc(3*m*sizeof(PetscInt),&cols);

	for (i = 0; i < m; ++i) {
			for (j = 0; j < 3; ++j) {
				a = T[3 * i + j];
				b = T[3 * i + ((j + 1) % 3)];
			    rows[3* i + j] = _min(a,b);
			    cols[3 *i + j] = _max(a,b);
			}
	}

	for(i=0;i<3*m;i++){
		cnt[rows[i]]=cnt[rows[i]]+1;
	}
	for(i=0;i<n;i++){if(cnt[i]) cnt[i]=(int)floor(cnt[i]/2)+1;}

	MatCreateSeqAIJ(PETSC_COMM_SELF,n,n,0,cnt,&A);
	for(i=0;i<3*m;i++){
	    MatSetValues(A,1,&rows[i],1,&cols[i],&one,ADD_VALUES);
	}
	MatAssemblyBegin(A,MAT_FINAL_ASSEMBLY);
	MatAssemblyEnd(A,MAT_FINAL_ASSEMBLY);
	//MatView(A,	PETSC_VIEWER_STDOUT_SELF);

	MatInfo info;
	double mem;
	MatGetInfo(A,MAT_LOCAL,&info);
	nz_a = (int)info.nz_allocated;
	nz_u = (int)info.nz_used;
	//mem=info.memory;
	//printf("%d %d %f\n",nz_a,nz_u,mem);
	//MatTranspose(A,MatReuse reuse,&AT);
	PetscFree(rows);
	PetscFree(cols);

	PetscInt **tempdege=malloc(nz_u*sizeof(PetscInt*));
	for(i=0;i<nz_u;i++){
		tempdege[i]=calloc(2,sizeof(PetscInt));
	}
	int count=0;
	for(i=0;i<n;i++){
		MatGetRow(A,i,&ncols,&cols1,&values);
        for(j=0;j<ncols;j++){
        	if(values[j]==one){
        		boundary[i]=1; boundary[cols1[j]]=1;
        		tempdege[count][0]=i;tempdege[count][1]=cols1[j];
        		count++;
        	}//else {tempdege[count][0]=i;tempdege[count][1]=cols1[i];}
        }
		MatRestoreRow(A,i,&ncols,&cols1,&values);
	}
	int* unique_list_edges=malloc(2*(count+1)*sizeof(int));
	for(i=0;i<count;i++){
		unique_list_edges[2*i]=_min(tempdege[i][0],tempdege[i][1]);
		unique_list_edges[2*i+1]=_max(tempdege[i][0],tempdege[i][1]);
	}

	//print_array_int(boundary,n,1);
	//print_array_int(unique_list_edges,2*count,2);

	//PetscFree(cols1);
	//PetscFree(values);
	MatDestroy(&A);
	//for(i=0;i<nz_u;i++) free(tempdege[i]);
	//free(tempdege);
	*boundaryf=boundary;
	*edge_boundary_list=unique_list_edges;
	*sizef=count;

}
int Tri_mesh_quality(int *T,int m,double *V,int n,double ** mesh_qualityf){
	double* mesh_quality=malloc(sizeof(int*)*m);
	double temp1,temp2,temp3;
	unsigned int i;
	double v11,v12,v21,v22,v31,v32;
	double Rmin,Rmax;

	for(i=0;i<m;i++){
	 v11=(V[2*T[3*i+1]]-V[2*T[3*i]]);
	 v12=(V[2*T[3*i+1]+1]-V[2*T[3*i]+1]);

	 v21=(V[2*T[3*i+2]]-V[2*T[3*i]]);
	 v22=(V[2*T[3*i+2]+1]-V[2*T[3*i]+1]);

	 v31=(V[2*T[3*i+2]]-V[2*T[3*i+1]]);
     v32=(V[2*T[3*i+2]+1]-V[2*T[3*i+1]+1]);

     temp1=sqrt(v11*v11+v12*v12); temp2=sqrt(v21*v21+v22*v22); temp3=sqrt(v31*v31+v32*v32);
     Rmin=0.5*sqrt((temp2+temp3-temp1)*(temp3+temp1-temp2)*(temp1+temp2-temp3)/(temp1+temp2+temp3));
     Rmax=(temp1*temp2*temp3)/sqrt((temp1+temp2+temp3)*(temp2+temp3-temp1)*(temp3+temp1-temp2)*(temp1+temp2-temp3));
     mesh_quality[i]=2.0*Rmin/Rmax;
	}
	*mesh_qualityf=mesh_quality;
	return 0;
}

void Is_mesh_good(double * mesh_quality,int m){
	qsort(mesh_quality,m,sizeof(double),compare_doubles);

	if(mesh_quality[0]>=0.5)  printf("Good Mesh quality \n");
	if(mesh_quality[0]<0.5 && mesh_quality[0]>0.1) printf("Moderate Mesh quality\n");
	if(mesh_quality[0]<=0.1) printf("Bad Mesh quality \n");
	printf("Minimum %f\n",mesh_quality[0]);
}

/*****************************************************************************/
/*                                                                           */
/* Function  delaunay2d creates the 2d dalaunay Triangulation of a two       */
/* dimentional mesh.                                                         */
/*                                                                           */
/* In oder to accomplice that we use the  following mesh generation code:    */
/*  A Two-Dimensional Quality Mesh Generator and Delaunay Triangulator.      */
/*  (triangle.c)                                                             */
/*                                                                           */
/*  Version 1.6                                                              */
/*  July 28, 2005                                                            */
/*                                                                           */
/*  Copyright 1993, 1995, 1997, 1998, 2002, 2005                             */
/*  Jonathan Richard Shewchuk                                                */
/*  2360 Woolsey #H                                                          */
/*  Berkeley, California  94705-1927                                         */
/*  jrs@cs.berkeley.edu                                                      */

int delaunay2d(double *nodes,int size_of_nodes,int *segments,int size_of_segments,\
	       double maximum_triangle_area,int min_angle,int **T, int *m, double **V, int *n,int **edgelist,int *nedges){
  struct triangulateio in, out;
  char *endptr;
  char control_string[80];
  in.numberofpoints = size_of_nodes;
  in.pointlist = nodes;
  in.pointmarkerlist = NULL;
  in.numberofpointattributes = 0;

  in.numberofsegments = size_of_segments;
  in.segmentlist = segments;
  in.segmentmarkerlist = NULL;

  in.numberofholes = 0;
  in.numberofregions = 0;

  out.pointlist = NULL;
  out.pointmarkerlist = NULL;

  out.edgelist = NULL;
  out.edgemarkerlist = NULL;

  out.trianglelist = NULL;
  out.triangleattributelist = NULL;

  out.segmentlist = NULL;
  out.segmentmarkerlist = NULL;
  sprintf(control_string, "Qzpeq%d a%g",min_angle,maximum_triangle_area);
  triangulate(control_string, &in, &out, NULL);
  fix_triangle_orientation(out.trianglelist,out.numberoftriangles,out.pointlist,out.numberofpoints);


  trifree(out.pointmarkerlist);
  trifree(out.edgemarkerlist);
  trifree(out.triangleattributelist);
  trifree(out.segmentlist);
  trifree(out.segmentmarkerlist);

  *T=out.trianglelist;
  *V=out.pointlist;
  *m=out.numberoftriangles;
  *n=out.numberofpoints;
  *edgelist=out.edgelist;
  *nedges=out.numberofedges;


  return 0;
}

typedef struct circle_info{
	double radius;
        double center_x;
        double center_y;
	int number_of_points;
	double maximum_triangle_area;
	int min_angle;
}circle_info;

int circular_mesh(int **T,int *m,double **V,int *n,const circle_info info)
{
	unsigned int i;
	double *nodes=malloc(sizeof(double)*2*info.number_of_points);
	int *segments=malloc(sizeof(double)*2*info.number_of_points);
	for(i=0;i<info.number_of_points;i++){
		nodes[2*i]=info.center_x+info.radius*cos(2*PI*i/(info.number_of_points));
		nodes[2*i+1]=info.center_y+info.radius*sin(2*PI*i/(info.number_of_points));
		segments[2*i]=i; segments[2*i+1]=(i+1)%info.number_of_points;
	}
	 int *edges;
	 int nedges;
	delaunay2d(nodes,info.number_of_points,segments,info.number_of_points,\
		     info.maximum_triangle_area,info.min_angle,T,m,V,n,&edges,&nedges);

	return 0;
}

typedef struct square_info{
	double corners[8];
	int segments[8];
	double maximum_triangle_area;
	int min_angle;
}square_info;

int square_mesh(int **T,int *m,double **V,int *n,square_info info){
	int *edges;
    int nedges;
	delaunay2d(info.corners,4,info.segments,4,\
			     info.maximum_triangle_area,info.min_angle,T,m,V,n,&edges,&nedges);

	return 0;
}


/*************************** Cumsum ***************************
 *  A function that computes the cumulative sum of an array.
 *
 */
int Cumsumfunc(int *array, int **Cumsum, int size) {
	unsigned int i;
	int* tempcumsum = malloc(sizeof(int) * (size));
	tempcumsum[0] = 0;
	for (i = 1; i < size; i++) {
		tempcumsum[i] = tempcumsum[i - 1] + array[i - 1];
	}
	*Cumsum = tempcumsum;
	return 0;
}


//**************** find_the_which_edges ************************
//puts -1 to tempedges for the corresponding boundary edges

int find_the_which_edges(int **I, int sizeI, int *tempedges, int sizedges,
		int*en, int sizen,int* edgelist,int size_list) {
	int i, b, count = 0,count1=0;
	b = -1;
	for (i = 0; i < sizen; i++) {
		/*printf("%d %d\n",I[i][0],I[i][1]);
		printf("%d %d\n",edgelist[2*count1],edgelist[2*count1+1]);
		printf("********** %d %d\n",en[i],b);*/
		if (en[i] != b) {
			if((I[i][0]==edgelist[2*count1]) && (I[i][1]==edgelist[2*count1+1]))
		    {
			/*printf("%d %d\n",I[i][0],I[i][1]);
			printf("%d %d\n",edgelist[2*count1],edgelist[2*count1+1]);
			printf("Yes-----\n");*/
		    tempedges[count]=-1;
		    count1=count1+1;
		    if(count1==size_list) return 0;
	    	}
			count = count + 1;
		}
		b = en[i];
	}
	return 0;

}


/************* Offset **************************************
 *  A function that finds the global_offset.
 *
 * Inputs:  T: triangles
 * 			m: number of triangles
 *			V: vertices
 *			n: number of vertices
 *			globaloffset: where the global offset is stored
 *			boundary: the natural boundary
 *			sizeboundary: size of the boundary
 *
 * Output:  globaloffset
 *
 *
 */
int offset(int *T, int m, double *V, int n, int **globalofset,
		int* boundary,int NEdges,int* edge_list,int size_list) {
	int i, j, a, b, NE, NI;
	int *tempvertices = malloc(sizeof(int) * n);
	int **I = malloc(sizeof(int *) * 6 * m);
	int *E = malloc(sizeof(int) * 6 * m);
	int *en = malloc(sizeof(int) * 3 * m);	//number the initial edges
	int *K = malloc(sizeof(int) * m);	//K will hold the global offset
	//This labels each vertex according to which triangle owns it
	for (i = m - 1; i > -1; i = i - 1) {
		for (j = 0; j < 3; j++) {
			tempvertices[T[3 * i + j]] = i;
		}
	}
	//We find the edges
	for (i = 0; i < m; ++i) {
		for (j = 0; j < 3; ++j) {
			a = T[3 * i + j];
			b = T[3 * i + ((j + 1) % 3)];
			E[6 * i + 2 * j] = _min(a,b);
			E[6 * i + 2 * j + 1] = _max(a,b);
		}
	}
	for (i = 0; i < m * 3; i++)
		I[i] = &E[2 * i];
	qsort(I, m * 3, sizeof(int *), lexorder2);
	en[0] = 0;
	for (i = 1; i < 3 * m; i++) {
		en[i] = en[i - 1] + (lexorder2(&I[i - 1], &I[i]) != 0);
	}
	int n1 = en[3 * m - 1] + 1;
	int *tempedges = malloc(sizeof(int) * n1);
	int *x = malloc(3 * m * sizeof(int));
	int k;
	for (i = 0; i < 3 * m; i++) {
		k = (I[i] - &E[0]) / 2;
		x[k] = en[i];
	}
	b = 0;

	for (i = 3 * m - 1; i > -1; i = i - 1) {
		tempedges[x[i]] = ceil(i / 3);
	}

	find_the_which_edges(I, 3 * m, tempedges, n1,en, 3 * m,edge_list,size_list);

	//We need to know how many times we will further subdivide
	//NE number of vertices on one edge after x refinements.
	//NE=1+pow(2,times_to_refine)-2;(for subdiv)
	NE = NEdges; //(for sequential mesh)
	NI = NE * (NE - 1) / 2;
	for (i = 0; i < m; i++) {
		K[i] = NI;
	}
	for (i = 0; i < n; i++) {
		if (!boundary[i]) {
			K[tempvertices[i]] = K[tempvertices[i]] + 1;
		}
	}
	for (i = 0; i < n1; i++) {
		if (tempedges[i] != -1) {
			K[tempedges[i]] = K[tempedges[i]] + NE;
		}
	}
	*globalofset = K;
	free(tempedges);
	free(E);
	free(I);
	free(en);
	free(tempvertices);

	return 0;
}

/************** Sequenctial_Triangle:**************************************
 *
 * A functions that creates a Triangle in a way that the vertices of the triangle
 * have a special enumeration that will help us with the Global offset.
 * Every edge of the triangle have "k" number of vertices.
 * Inputs:
 * 		T:initial triangle
 * 		V:initial Vertices
 * 		n: size of V
 * 		k: number of vertices on each edge
 * Output:
 * 		Tloc:Local "refined" Triangles
 * 		sizeTloc: size of Tloc
 * 		Vloc:Local "refined" Vertices
 * 		sizeVloc: size of Vloc
 */
int Sequenctial_Triangle(int *T, double *V, int sizeV, int k, int **Tloc,
		int*sizeTloc, double** Vloc, int* sizeVloc) {
	int n0 = 3+3*k+(k*(k-1)/2), i, j, q, count = 0, countT = 0;
	int m0 = 9 + 6 * (k - 2) + pow((k - 2), 2);
	double* Vloc0 = malloc(2 * sizeof(double) * n0);
	int* Tloc0 = malloc(3 * sizeof(int) * (m0 + 1));
	double  *u = malloc(sizeof(double) * 2),\
			*v = malloc(sizeof(double) * 2); //* Vtemp = malloc(6 * sizeof(double)),
	int array[] = { 0, 1, 2 }, temp = 0;
	int* b = malloc(sizeof(int) * (k + 2));
	int* d = malloc(sizeof(int) * (k + 1));
	int* finaledge = malloc(sizeof(int) * (2 * (k+2)));
	int* edge2 = malloc(sizeof(int) * k);
	int* edge3 = malloc(sizeof(int) * k);

	//***First we create the vertices***
	for (i = 0; i < 3; i++) {
		Vloc0[2 * i] = V[2 * T[i]];
		Vloc0[2 * i + 1] = V[2 * T[i] + 1];
	}
	count = 3;
	for (i = 0; i < 3; i++) {
		q = (i + 1) % 3;
		u[0] = V[2 * T[i]];
		u[1] = V[2 * T[i] + 1];
		v[0] = V[2 * T[q]];
		v[1] = V[2 * T[q] + 1];
		for (j = 1; j < k + 1; j++) {
			Vloc0[2 * count] = (j * v[0] + (k + 1 - j) * u[0]) / (k + 1);
			Vloc0[2 * count + 1] = (j * v[1] + (k + 1 - j) * u[1]) / (k + 1);
			count = count + 1;
		}
	}

	u[0] = V[2 * T[1]] - V[2 * T[0]];
	u[1] = V[2 * T[1] + 1] - V[2 * T[0] + 1];
	v[0] = V[2 * T[2]] - V[2 * T[0]];
	v[1] = V[2 * T[2] + 1] - V[2 * T[0] + 1];
	for (i = 1; i <= k - 1; i++) {
		for (j = 1; j <= (k - i); j++) {
			Vloc0[2 * count] = V[2 * T[0]] + ((j * 1.0) / (k + 1)) * u[0]
					+ ((i * 1.0) / (k + 1)) * v[0];
			Vloc0[2 * count + 1] = V[2 * T[0] + 1]
					+ ((j * 1.0) / (k + 1)) * u[1]
					+ ((i * 1.0) / (k + 1)) * v[1];
			count = count + 1;
		}
	}
	/*** Next we create the Triangles***/
	b[0] = array[0];
	for (i = 1; i < k + 1; i++) {
		b[i] = 3 + i - 1;
	}
	b[k + 1] = array[1];
	for (i = 0; i < k; i++) {
		d[i] = 3 * k + 2 + i;
	}
	d[k] = k + 3;
	for (i = 0; i < k; i++) {
		edge2[i] = k + 3 + i;
		edge3[i] = 2 * k + 3 + i;
	}
	for (i = 0; i < k; i++) {
		finaledge[2 * i] = edge3[k - i - 1];
		finaledge[2 * i + 1] = edge2[i];
	}

	for (i = 0; i < k; i++) {
		if (i == 0) {
			Tloc0[3 * countT] = b[i];
			Tloc0[3 * countT + 1] = b[i + 1];
			Tloc0[3 * countT + 2] = d[i];
			countT = countT + 1;
			Tloc0[3 * countT] = b[i + 1];
			Tloc0[3 * countT + 1] = d[i];
			Tloc0[3 * countT + 2] = d[i + 1];
			countT = countT + 1;
		} else {
			Tloc0[3 * countT] = b[i];
			Tloc0[3 * countT + 1] = b[i + 1];
			Tloc0[3 * countT + 2] = d[i];
			countT = countT + 1;
			Tloc0[3 * countT] = b[i + 1];
			Tloc0[3 * countT + 1] = d[i];
			Tloc0[3 * countT + 2] = d[i + 1];
			countT = countT + 1;
		}
	}
	Tloc0[3 * countT] = b[k];
	Tloc0[3 * countT + 1] = b[k] + 1;
	Tloc0[3 * countT + 2] = b[k + 1];
	temp = d[k - 1];

	for (j = 2; j < k + 2; j++) {
		for (i = 0; i < k - j + 3; i++) {
			b[i] = d[i];
		}
		countT = countT + 1;
		d[0] = finaledge[2 * (j - 1)];
		//printf("D0 : %d\n",d[0]);
		for (i = 1; i < k - j + 1; i++) {
			d[i] = temp + i;
		}
		d[k - j + 1] = finaledge[2 * (j - 1) + 1];
		for (i = 0; i < k - j + 1; i++) {
			Tloc0[3 * countT] = b[i];
			Tloc0[3 * countT + 1] = b[i + 1];
			Tloc0[3 * countT + 2] = d[i];
			countT = countT + 1;
			Tloc0[3 * countT] = b[i + 1];
			Tloc0[3 * countT + 1] = d[i];
			Tloc0[3 * countT + 2] = d[i + 1];
			countT = countT + 1;
		}
		temp = temp + k - j;
		Tloc0[3 * countT] = b[k - j + 1];
		Tloc0[3 * countT + 1] = finaledge[2 * (j - 2) + 1];
		Tloc0[3 * countT + 2] = finaledge[2 * (j - 2) + 1] + 1;
	}
	//countT=countT+1;
	Tloc0[3 * countT] = finaledge[2 * (k - 1)];
	Tloc0[3 * countT + 1] = finaledge[2 * (k - 1) + 1];
	Tloc0[3 * countT + 2] = 2;

	free(b);
	free(d);
	free(u);
	free(v);
	free(finaledge);
	free(edge2);
	free(edge3);
	*Tloc = Tloc0;
	*sizeTloc = countT;
	*Vloc = Vloc0;
	*sizeVloc = count;
	*sizeTloc = m0;
	//*sizeVloc = n0;
	return 0;
}



/************* find_previews_neighbours************************
 * A function that find all the previous triangles that are neighbours
 * of master triangle.
 * inputs:
 *
 * 		Tcoarse: Triangles of coarse mesh
 * 		sizeTcoarse: number triangles of coarse mesh
 * 		Tmaster: Master triangle, i.e Tmaster=[1 2 5];
 * 		proc : which triangle (corresponds to processor
 * out:
 * 		neib:  array that will hold the neighbours
 * 		sizeneb: number of neigbours with proc<procmaster;
 *      belong0: belong[i]=1 if vertex belong to triangle 0 else;
 */
/************* find_previews_neighbours************************
 * A function that find all the previous triangles that are neighbours
 * of master triangle.
 * inputs:
 *
 * 		Tcoarse: Triangles of coarse mesh
 * 		sizeTcoarse: number triangles of coarse mesh
 * 		Tmaster: Master triangle, i.e Tmaster=[1 2 5];
 * 		proc : which triangle (corresponds to processor
 * out:
 * 		neib:  array that will hold the neighbours
 * 		sizeneb: number of neigbours with proc<procmaster;
 *      belong0: belong[i]=1 if vertex belong to triangle 0 else;
 */
int find_previews_neighbours(int* Tcoarse,int* Tmaster,
		int proc, int* neib, int* sizeneb, int *belong0, int* belong) {
	int i, j;
	int sizeneb0 = 0;
	int a, b, c;
	int Tcoarsetemp[3];
	for (i = 0; i < 3; i++) {
		belong0[i] = 1;
		belong[i] = 1;
	}
	for (i = 0; i < proc; i++) {
		Tcoarsetemp[0] = Tcoarse[3 * i];
		Tcoarsetemp[1] = Tcoarse[3 * i + 1];
		Tcoarsetemp[2] = Tcoarse[3 * i + 2];
		//printf("Tmaster %d %d %d\n",Tmaster[0],Tmaster[1],Tmaster[2]);
		//printf("Tslave %d %d %d\n",Tcoarsetemp[0],Tcoarsetemp[1],Tcoarsetemp[2]);
		a = linearSearchInt(Tcoarsetemp, Tmaster[0], 0, 3);
		b = linearSearchInt(Tcoarsetemp, Tmaster[1], 0, 3);
		c = linearSearchInt(Tcoarsetemp, Tmaster[2], 0, 3);
		for (j = 0; j < 3; j++) {
			if ((a != -1) && (b != -1) && (belong[0] == 1)) {
				belong[0] = 0;
			}
			if ((b != -1) && (c != -1) && (belong[1] == 1)) {
				belong[1] = 0;
			}
			if ((a != -1) && (c != -1) && (belong[2] == 1)) {
				belong[2] = 0;
			}
			if ((a != -1) || (b != -1) || (c != -1)) {
				neib[sizeneb0] = i;
				sizeneb0 = sizeneb0 + 1;
				break;
			}
		}
		if ((a != -1) && (belong0[0] == 1)) {
			belong0[0] = 0;
		}
		if ((b != -1) && (belong0[1] == 1)) {
			belong0[1] = 0;
		}
		if ((c != -1) && (belong0[2] == 1)) {
			belong0[2] = 0;
		}
	}

	*sizeneb = sizeneb0;
	return 0;
}


/******************** Flag_Non_Verts *******************************
 *  A function that puts zero when the vertex doesn't belong to
 *  triangle.
 *
 *   input:
 *   		flags: where the type of vertices are stored
 *   		sizeflags: size of the vertsarray.
 *   		belong: binary array that stores if the edge belong to the edge
 *   		NE: Number of vertices per edge
 *	 output:
 *	 		 corrected versarray, 0's when the vertex deosn't belong to the triangle
 */

int Flag_Non_Verts(int* flags, int sizeflags, int *belong, int NE) {
	int i;
	if (belong[0] == 0) {
		for (i = 3; i < 3 + NE; i++) {
			flags[i] = 0;
		}
	}
	if (belong[1] == 0) {
		for (i = 3 + NE; i < 3 + 2 * NE; i++) {
			flags[i] = 0;
		}
	}
	if (belong[2] == 0) {
		for (i = 3 + 2 * NE; i < 3 + 3 * NE; i++) {
			flags[i] = 0;
		}
	}

	return 0;

}
//Check function for Global offset use//
int check_if_edge_on_boundary(int *edgelist,int size_list,int a1,int b1){
	int a,b,i;
	a=_min(a1,b1);
	b=_max(a1,b1);

	for(i=0;i<size_list;i++){
		if(a==edgelist[2*i]){
			if(b==edgelist[2*i+1]){
				return 1;
			}
		}
	}
	return 0;
}


int Help_offset(int* Tcoarse,int sizeTcoarse,int sizeVmaster, int** MasterOffset,
		int NE, int cumsum, int* boundary, int proc,int*edge_list,int size_list,int* cunsumarray) {

	int* id = malloc(sizeof(int) * sizeVmaster);
	int* flags = malloc(sizeof(int) * sizeVmaster);
	int tempbound[3];
	int Tmaster[3], neib[20], belongedge[3],belongvert[3];
	int i, sizeneib;
	int count = 0;
	//int interior_points = NEdges * (NEdges - 1) / 2;
	/*we want to put -2 when we have boundary -1 if we don't know, 1 if belongs
	 *to the triangle (we know that the interior belong to the triangle)
	 id is -1 the vertex does not belong to the boundary -2 if we dont know*/

	Tmaster[0] = Tcoarse[3 * proc];
	Tmaster[1] = Tcoarse[3 * proc + 1];
	Tmaster[2] = Tcoarse[3 * proc + 2];

	//Initialise we know that interior nodes belong to this subdomain.
	for (i = 0; i <3 * NE + 3; i++) { // We don't know
		flags[i] = -1;
		id[i] = -2;
	}
	for (i = 3 * NE + 3; i<sizeVmaster; i++) { // We know
		flags[i] = 1;
		id[i] = cumsum + count;
		count = count + 1;
	}

	//Check if the vertices are on the boundary
	for (i = 0; i < 3; i++) {
		if (boundary[Tmaster[i]]==1) {
			flags[i] = -2;
			id[i] = -1;
		}
	}

	//Finally Check if some edges belong on the boundary(I will make a simple function for that) //
	if(check_if_edge_on_boundary(edge_list,size_list,Tmaster[0],Tmaster[1])){
		for (i = 3; i < 3+NE; i++) {
			flags[i] = -2;
			id[i] = -1;
		}
	}
	if(check_if_edge_on_boundary(edge_list,size_list,Tmaster[1],Tmaster[2])){
		for (i = 3+NE; i < 3+2*NE; i++) {
			flags[i] = -2;
			id[i] = -1;
		}
	}
	if(check_if_edge_on_boundary(edge_list,size_list,Tmaster[2],Tmaster[0])){
		for (i = 3+2*NE; i < 3+3*NE; i++) {
			flags[i] = -2;
			id[i] = -1;
		}

	}

	//It is essenstial to set the values for triangle(subdomain 0)//
	if (proc == 0) {
		count=1;
		for (i = 0; i < sizeVmaster; i++) {
			if (flags[i] == -1) {
				flags[i] = 1;
				id[i] = id[sizeVmaster - 1] + count;
				count = count + 1;
			}
		}
		*MasterOffset = id;
		free(flags);
		return 0;
	}


	//GOOD until here now the tricky part ... //

	//To Find the global offset of Processor 0 was an easy task now we have
	// to find the global offset for the rest processes.

	find_previews_neighbours(Tcoarse,&Tcoarse[3*proc],
			proc, neib,&sizeneib,belongvert,belongedge);
	Flag_Non_Verts(flags, sizeVmaster,belongedge, NE);


	for (i = 0; i < 3; i++) {
		if ((belongvert[i] == 0) && (flags[i] == -1)) {
			flags[i] = 0;
			id[i] = -1;
		}
	}
	if (sizeneib == 0) {
		count=1;
		for (i = 0; i <  3 * NE + 3; i++) {
			if (flags[i] == -1) {
				flags[i] = 1;
				id[i] = id[sizeVmaster - 1] + count;
				count = count + 1;
			}
		}
	}else{
		count=1;
		for (i = 3; i < 3 * NE + 3; i++) {
			if (flags[i] == -1) {
				flags[i] = 1;
				id[i] = id[sizeVmaster - 1] + count;
				count = count + 1;
			}
		}
	}
	for (i = 0; i < 3; i++) {
			if (flags[i] == -1 && id[i] == -2) {
				flags[i] = 1;
				id[i] = id[sizeVmaster - 1] + count;
				count=count+1;
			}
     }

	*MasterOffset = id;
	free(flags);
	return 0;
}


/****************** find Rem s2 *********************************
 * Improved version of find_Rem_s1
 * Find remaining vertices.
 * We need to compare the master triangle with it's neighbours
 * in order to find the global offset.
 *
 * input:
 * 		neid:neibours with smaller values
 * 		sizeneid: size of neib
 * 		Tcoarse:coarse Triangles
 * 		sizeTcoarse: size of Tcoarse array
 * 		Vcoarse: coordinates of the vertices of the coarse Triangles
 * 		sizeVcoarse: size of Vcoarse array
 * 		Tmaster: corner vertices of the master triangle
 * 		Vmaster: coordinates of the master Triangle
 * 		sizeVmaster: coordinates of the master Triangle.
 * 		NEdges: number of vertices per edge
 * 		Kcumsum: cumulative sum used to offset
 * 		Neib: neighbour that we want to check
 * 		flags: flag 0 1 -1
 *
 * output:
 *       id: filled with more values
 *
 */

typedef enum{
	NC,FF11,FF12,FF13,FF21,FF22,FF23,FF31,FF32,FF33,\
	   FB11,FB12,FB13,FB21,FB22,FB23,FB31,FB32,FB33
}EdgeType;


int find_Rem_s2(int* neib, int sizeneib, int* Tcoarse, int sizeTcoarse,
		int* Tmaster, int sizeVmaster, int* id, int NEdges, int* cumsum, int* boundary,
		int* flags,int* edge_list,int size_list) {

	int i,m,n;
	int count1,countflag,temparray2[6];//count,
	int* Neiboffset;
	int TNeib[3], n1neib;

	EdgeType Etype[3];

	for (i = 0; i < sizeneib; i++) {
		//count = 0;
		countflag = 0;
		count1=0;
		n1neib=sizeVmaster;

		TNeib[0] = Tcoarse[3 * neib[i]];
		TNeib[1] = Tcoarse[3 * neib[i] + 1];
		TNeib[2] = Tcoarse[3 * neib[i] + 2];

	   Help_offset(Tcoarse,sizeTcoarse,sizeVmaster, &Neiboffset,
			NEdges, cumsum[neib[i]], boundary,neib[i],edge_list,size_list,cumsum);
		//Find how many zero are in flags (How many unknown vertices)
		for (m = 0; m < n1neib; m++) {
			if (flags[m] == 0) {
				countflag = countflag + 1;
			}
		}

		/*Here we assign the connection between the edges of the Matster
		 * and the Slave triangle. ie 0-0 means the the 0 vertex of
		 * the two triangles match. If we also have 1-1  the the edge
		 * 0-1 is the same for the two triangle and with common orientation.
		 * We do that in order to assigned the vertices.
		 */
		//printf("HERE: %d %d %d : %d %d %d\n",Tmaster[0],Tmaster[1],Tmaster[2],TNeib[0],TNeib[1],TNeib[2]);
		if (Tmaster[0] == TNeib[0]) {
			temparray2[2 * count1] = 0;
			temparray2[2 * count1 + 1] = 0;
			count1 = count1 + 1;
		}
		if (Tmaster[0] == TNeib[1]) {
			temparray2[2 * count1] = 0;
			temparray2[2 * count1 + 1] = 1;
			count1 = count1 + 1;
		}
		if (Tmaster[0] == TNeib[2]) {
			temparray2[2 * count1] = 0;
			temparray2[2 * count1 + 1] = 2;
			count1 = count1 + 1;
		}
		if (Tmaster[1] == TNeib[0]) {
			temparray2[2 * count1] = 1;
			temparray2[2 * count1 + 1] = 0;
			count1 = count1 + 1;
		}
		if (Tmaster[1] == TNeib[1]) {
			temparray2[2 * count1] = 1;
			temparray2[2 * count1 + 1] = 1;
			count1 = count1 + 1;
		}
		if (Tmaster[1] == TNeib[2]) {
			temparray2[2 * count1] = 1;
			temparray2[2 * count1 + 1] = 2;
			count1 = count1 + 1;
		}
		if (Tmaster[2] == TNeib[0]) {
			temparray2[2 * count1] = 2;
			temparray2[2 * count1 + 1] = 0;
			count1 = count1 + 1;
		}
		if (Tmaster[2] == TNeib[1]) {
			temparray2[2 * count1] = 2;
			temparray2[2 * count1 + 1] = 1;
			count1 = count1 + 1;
		}
		if (Tmaster[2] == TNeib[2]) {
			temparray2[2 * count1] = 2;
			temparray2[2 * count1 + 1] = 2;
			count1 = count1 + 1;
		}

		//Check if we can fill vertex with the one of neib//
		for (m = 0; m < count1; m++) {
			if ((flags[temparray2[2 * m]] == 0)
					&& (Neiboffset[temparray2[2 * m + 1]] > -1)) {
				id[temparray2[2 * m]] = Neiboffset[temparray2[2 * m + 1]];
				flags[temparray2[2 * m]] = 1;
				countflag = countflag - 1;
			}
		}
		if (countflag == 0) {
			return 0;
		}		//check if we have finished
		//printf("countflag: %d",countflag);

		 /*THE HARD PART WE NEED TO FIND THE ORIENTATION OF THE TRIANGLES
		 * AND ASSINGED THE CORRECT VERTICES ON THE EDGES.*/

		//*FIRST EDGE
		Etype[0]=NC; Etype[1]=NC; Etype[2]=NC;

		if ((Tmaster[0] == TNeib[0]) && (Tmaster[1]==TNeib[1])){
			Etype[0]=FF11;}
		if ((Tmaster[0] == TNeib[1]) && (Tmaster[1]==TNeib[0])){
			Etype[0]=FB11;}
		if ((Tmaster[0] == TNeib[1]) && (Tmaster[1]==TNeib[2])){
			Etype[0]=FF12;}
		if ((Tmaster[0] == TNeib[2]) && (Tmaster[1]==TNeib[1])){
			Etype[0]=FB12;}
		if ((Tmaster[0] == TNeib[2]) && (Tmaster[1]==TNeib[0])){
			Etype[0]=FF13;}
		if ((Tmaster[0] == TNeib[0]) && (Tmaster[1]==TNeib[2])){
			Etype[0]=FB13;}

		//*SECOND EDGE
		if ((Tmaster[1] == TNeib[1]) && (Tmaster[2]==TNeib[2])){
			Etype[1]=FF22;}
		if ((Tmaster[1] == TNeib[2]) && (Tmaster[2]==TNeib[1])){
			Etype[1]=FB22;}
		if ((Tmaster[1] == TNeib[2]) && (Tmaster[2]==TNeib[0])){
			Etype[1]=FF23;}
		if ((Tmaster[1] == TNeib[0]) && (Tmaster[2]==TNeib[2])){
			Etype[1]=FB23;}
		if ((Tmaster[1] == TNeib[0]) && (Tmaster[2]==TNeib[1])){
			Etype[1]=FF21;}
		if ((Tmaster[1] == TNeib[1]) && (Tmaster[2]==TNeib[0])){
			Etype[1]=FB21;}

		//*THIRD EDGE
		if ((Tmaster[2] == TNeib[2]) && (Tmaster[0]==TNeib[0])){
			Etype[2]=FF33;}
		if ((Tmaster[2] == TNeib[0]) && (Tmaster[0]==TNeib[2])){
			Etype[2]=FB33;}
		if ((Tmaster[2] == TNeib[0]) && (Tmaster[0]==TNeib[1])){
			Etype[2]=FF31;}
		if ((Tmaster[2] == TNeib[1]) && (Tmaster[0]==TNeib[0])){
			Etype[2]=FB31;}
		if ((Tmaster[2] == TNeib[1]) && (Tmaster[0]==TNeib[2])){
			Etype[2]=FF32;}
		if ((Tmaster[2] == TNeib[2]) && (Tmaster[0]==TNeib[1])){
			Etype[2]=FB32;}

		//printf("Etype %d %d %d\n",Etype[0],Etype[1],Etype[2]);
		//* NOW TO THE MAIN DISH
		for (n = 0; n < count1 - 1; n++) {
			//* Set the first Edge
			if (flags[3] == 0) {
				if(Etype[0]==FF11){
					for (m = 3; m < 3 + NEdges; m++) {
						id[m] = Neiboffset[m];\
						flags[m] = 1;
						countflag = countflag - 1;
					}
				}
				if(Etype[0]==FB11){
					for (m = 3; m < 3 + NEdges; m++) {
						id[m] = Neiboffset[5 + NEdges - m];\
						flags[m] = 1;
						countflag = countflag - 1;
					}

				}
				if(Etype[0]==FF12){
					for (m = 3; m < 3 + NEdges; m++) {
						id[m] = Neiboffset[NEdges + m];\
						flags[m] = 1;
						countflag = countflag - 1;
					}
				}
				if(Etype[0]==FB12){
					for (m = 3; m < 3 + NEdges; m++) {
						id[m] = Neiboffset[5 + 2 * NEdges - m];\
						flags[m] = 1;
						countflag = countflag - 1;
					}
				}
				if(Etype[0]==FF13){
					for (m = 3; m < 3 + NEdges; m++) {
						id[m] = Neiboffset[2 * NEdges + m];\
						flags[m] = 1;
						countflag = countflag - 1;
					}
				}

				if(Etype[0]==FB13){
					for (m = 3; m < 3 + NEdges; m++) {
						id[m] = Neiboffset[5 + 3 * NEdges - m];\
						flags[m] = 1;
						countflag = countflag - 1;
					}
				}
			}
			//* Set the second Edge
			if (flags[3 + NEdges] == 0) {
				if(Etype[1]==FF21){
					for (m = 3; m < 3 + NEdges; m++) {
						id[3 + NEdges + (m - 3)] = Neiboffset[m];\
						flags[3 + NEdges + (m - 3)] = 1;
						countflag = countflag - 1;
					}
				}
				if(Etype[1]==FB21){
					for (m = 3; m < 3 + NEdges; m++) {
						id[3 + NEdges + (m - 3)] = Neiboffset[5 + NEdges- m];\
						flags[3 + NEdges + (m - 3)] = 1;
						countflag = countflag - 1;
					}
				}
				if(Etype[1]==FF22){
					for (m = 3; m < 3 + NEdges; m++) {
						id[3 + NEdges + (m - 3)] =Neiboffset[NEdges + m];\
						flags[3 + NEdges + (m - 3)] = 1;
						countflag = countflag - 1;
					}
				}
				if(Etype[1]==FB22){
					for (m = 3; m < 3 + NEdges; m++) {
						id[3 + NEdges + (m - 3)] = Neiboffset[5+ 2 * NEdges - m];\
						flags[3 + NEdges + (m - 3)] = 1;
						countflag = countflag - 1;
					}
				}
				if(Etype[1]==FF23){
					for (m = 3; m < 3 + NEdges; m++) {
						id[3 + NEdges + (m - 3)] = Neiboffset[2 * NEdges + m];\
						flags[3 + NEdges + (m - 3)] = 1;
						countflag = countflag - 1;
					}
				}
				if(Etype[1]==FB23){
					for (m = 3; m < 3 + NEdges; m++) {
						id[3 + NEdges + (m - 3)] = Neiboffset[5 + 3 * NEdges - m];
						flags[3 + NEdges + (m - 3)] = 1;
						countflag = countflag - 1;
					}
				}
			}
			//* Set the third Edge
			if (flags[3 + 2 * NEdges] == 0){

				if(Etype[2]==FF31){
					for (m = 3; m < 3 + NEdges; m++) {
						id[3 + 2 * NEdges + (m - 3)] = Neiboffset[m];\
						flags[3 + 2 * NEdges + (m - 3)] = 1;
						countflag = countflag - 1;
					}
				}
				if(Etype[2]==FB31){
					for (m = 3; m < 3 + NEdges; m++) {
						id[3 + 2 * NEdges + (m - 3)] = Neiboffset[5	+ NEdges - m];\
						flags[3 + 2 * NEdges + (m - 3)] = 1;
						countflag = countflag - 1;
					}
				}
				if(Etype[2]==FF32){
					for (m = 3; m < 3 + NEdges; m++) {
						id[3 + 2 * NEdges + (m - 3)] = Neiboffset[NEdges + m];\
						flags[3 + 2 * NEdges + (m - 3)] = 1;
						countflag = countflag - 1;
					}
				}
				if(Etype[2]==FB32){
					for (m = 3; m < 3 + NEdges; m++) {
						id[3 + 2 * NEdges + (m - 3)] = Neiboffset[5 + 2 * NEdges - m];\
						flags[3 + 2 * NEdges + (m - 3)] = 1;
						countflag = countflag - 1;
					}
				}
				if(Etype[2]==FF33){
					for (m = 3; m < 3 + NEdges; m++) {
						id[3 + 2 * NEdges + (m - 3)] = Neiboffset[2* NEdges + m];\
						flags[3 + 2 * NEdges + (m - 3)] = 1;
						countflag = countflag - 1;
					}
				}
				if(Etype[2]==FB33){
					for (m = 3; m < 3 + NEdges; m++) {
						id[3 + 2 * NEdges + (m - 3)] = Neiboffset[5+ 3 * NEdges - m];\
						flags[3 + 2 * NEdges + (m - 3)] = 1;
						countflag = countflag - 1;
					}
				}
			}


			if (countflag == 0) {
				return 0;
			}

		}
	}
	return 0;
}



/******************* find_sizeof_boundary *******************************
 *      A function that finds the size of boundary for the local mesh
 *      works with Sequencial_triangle. We need to check only the 3+NE-1
 *      first elements of the offset array.
 *
 *   inputs:
 *   		NEdged: number of vertices per edge
 *   		OffsetArray: array with the offsets has a number <0 where
 *   	    there is a boundary.
 *   output: size of the boundary.
 *
 */
int find_sizeof_boundary(int NEdge, int* offsetArray, int* sizeboundary) {
	int i, count, *temparray;
	count = 0;
	temparray = malloc(sizeof(int) * (3 + 3 * NEdge + 1));
	for (i = 0; i < (3 + (3 * NEdge)); i++) {
		temparray[i] = offsetArray[i];
	}
	qsort(temparray, 3 + 3 * NEdge, sizeof(int),compareints);
	for (i = 0; i < (3 + 3 * NEdge); i++) {
		if (temparray[i] == -1) {
			count = count + 1;
		} else {
			break;
		}
	}
	*sizeboundary = count;
	free(temparray);
	return 0;
}



/********************** Renumber_Verts ***********************************
 *  When we will assemble the stiffness Matrix for the Local mesh we
 *  the numbering will be from 0 to sizeofV but since some vertices corespond
 *  to the boundary we should renumber the interior vertices
 * 	input:
 * 		 flags: flags if the vertex belongs 1 on the interior or -2 the boundary
 * 		 sizeflags: size of flags array
 * 	output:
 * 		renumber_verts: contains new numbering for the vertex.
 *
 */
int Renumber_Verts(int*flags, int sizeflgas, int**renumber_verts) {
	int i, count = 0;
	int *renumber = malloc(sizeof(int) * sizeflgas);
	for (i = 0; i < sizeflgas; i++){renumber[i] = -1;}
	for (i = 0; i < sizeflgas; i++) {
		if (flags[i] > -1) {
			renumber[i] = count;
			count = count + 1;
		}
	}
	*renumber_verts = renumber;
	return 0;
}


/********************* set_Global_offset***********************
 * A function that sets the global offset for the "master" triangle.
 * This function should work with structure and unstructured mesh(not checked yet).
 * input:
 * 		Vmaster: Coordinates of master triangle Vertices
 * 		sizeVmaster: size of Vmaster array
 * 		Tcoarse: Coarse Triangles
 * 		sizeTcoarse: size  of Tcoarse array
 * 		Vcoarse: Coordinates of coarse mesh
 * 		sizeVcoarse: size of Vcoarse
 *		NEdgse: Number of vertices per edge
 *		Kcumsum: cumsum matrix used for the offset
 *		boundary: boundary of of coarse mesh
 *		second: size of boundary
 *		proc: which proc (Triangle) is the master
 *		ntcorner: next to corner vertices
 *		sizentcorner: size ntcorner
 *		Kcumsumarray:  full Kcumsum array
 *
 *	output:
 *		 MasterOffset: The offset for the master Triangle.
 *
 * function used:
 *     find_previews_neighbours
 *     LinearSearch_V_Bound
 *     Correct_V_Bound
 *     find_previews_neighbours
 *     Set_NonVerts
 *
 *
 *
 */
int set_Global_offset(int* Tcoarse,int sizeTcoarse,int sizeVmaster, int** MasterOffset,
		int NE, int cumsum, int* boundary, int proc,int*edge_list,int size_list,int* cunsumarray) {

	int* id = malloc(sizeof(int) * sizeVmaster);
	int* flags = malloc(sizeof(int) * sizeVmaster);
	//int* neibloc = malloc(sizeof(int) * 12);
	int tempbound[3];
	int Tmaster[3], neib[20], belongedge[3],belongvert[3];
	int i, sizeneib;
	int count = 0;
	//int interior_points = NEdges * (NEdges - 1) / 2;
	/*we want to put -2 when we have boundary -1 if we don't know, 1 if belongs
	 *to the triangle (we know that the interior belong to the triangle)
	 id is -1 the vertex does not belong to the boundary -2 if we dont know*/

	Tmaster[0] = Tcoarse[3 * proc];
	Tmaster[1] = Tcoarse[3 * proc + 1];
	Tmaster[2] = Tcoarse[3 * proc + 2];

	//Initialize we know that interior nodes belong to this subdomain.
	for (i = 0; i <3 * NE + 3; i++) { // We don't know
		flags[i] = -1;
		id[i] = -2;
	}
	for (i = 3 * NE + 3; i<sizeVmaster; i++) { // We know
		flags[i] = 1;
		id[i] = cumsum + count;
		count = count + 1;
	}

	//Check if the vertices are on the boundary
	for (i = 0; i < 3; i++) {
		if (boundary[Tmaster[i]]==1) {
			flags[i] = -2;
			id[i] = -1;
		}
	}

	//Finally Check if some edges belong on the boundary(I will make a simple function for that) //
	if(check_if_edge_on_boundary(edge_list,size_list,Tmaster[0],Tmaster[1])){
		for (i = 3; i < 3+NE; i++) {
			flags[i] = -2;
			id[i] = -1;
		}
	}
	if(check_if_edge_on_boundary(edge_list,size_list,Tmaster[1],Tmaster[2])){
		for (i = 3+NE; i < 3+2*NE; i++) {
			flags[i] = -2;
			id[i] = -1;
		}
	}
	if(check_if_edge_on_boundary(edge_list,size_list,Tmaster[2],Tmaster[0])){
		for (i = 3+2*NE; i < 3+3*NE; i++) {
			flags[i] = -2;
			id[i] = -1;
		}

	}

	//for (i = 0; i < sizeVmaster; i++) {
	//		printf("%d %d %d %d\n", id[i], flags[i],cumsum,sizeVmaster);}

	//It is essenstial to set the values for triangle(subdomain 0)//
	if (proc == 0) {
		count=1;
		for (i = 0; i < sizeVmaster; i++) {
			if (flags[i] == -1) {
				flags[i] = 1;
				id[i] = id[sizeVmaster - 1] + count;
				count = count + 1;
			}
		}
		*MasterOffset = id;
		free(flags);
		return 0;
	}


	//GOOD until here now the tricky part ... //

	//To Find the global offset of Processor 0 was an easy task now we have
	// to find the global offset for the rest processes.

	find_previews_neighbours(Tcoarse,&Tcoarse[3*proc],
			proc, neib,&sizeneib,belongvert,belongedge);
	Flag_Non_Verts(flags, sizeVmaster,belongedge, NE);


	for (i = 0; i < 3; i++) {
		if ((belongvert[i] == 0) && (flags[i] == -1)) {
			flags[i] = 0;
			id[i] = -1;
		}
	}
	if (sizeneib == 0) {
		count=1;
		for (i = 0; i <  3 * NE + 3; i++) {
			if (flags[i] == -1) {
				flags[i] = 1;
				id[i] = id[sizeVmaster - 1] + count;
				count = count + 1;
			}
		}
	}else{
		count=1;
		for (i = 3; i < 3 * NE + 3; i++) {
			if (flags[i] == -1) {
				flags[i] = 1;
				id[i] = id[sizeVmaster - 1] + count;
				count = count + 1;
			}
		}
	}

	find_Rem_s2(neib,sizeneib,Tcoarse,sizeTcoarse,
			Tmaster,sizeVmaster,id,NE, cunsumarray, boundary,
			flags,edge_list,size_list);
	for (i = 0; i < 3; i++) {
			if (flags[i] == -1 && id[i] == -2) {
				flags[i] = 1;
				id[i] = id[sizeVmaster - 1] + count;
				count=count+1;
			}
	}

	*MasterOffset = id;
	free(flags);
	return 0;
}



int Proc_Stiffnes_Mat(int rank, int NEdges, int Subdivisions,
		double** PMAT_Stiff, int* sizePMAT, int** Global_columns,
		int** Global_rows, double** gfvalues, int** gfrows, int* sizef,
		int** Goffset, int* sizeofbound, int*sizeV,int** renumber) {

	int T0[] = {0,1,2,
			0,2,3};
	double V0[] = {0,0,
			1,0,
			1,1,
			0,1};
	int m=2,n=4;
	int *T1 = &T0[0],ref=1;
	//int m,n;
	//int *T1,ref=1;
	int *edges;
	int nedges;

	//double *V1;
	double *V1 = &V0[0];
	unsigned int i;
	int *boundary,size;
	//Uniform mesh refinement//
	for(i=0;i<Subdivisions;i++){
	  uniform(&T1,&m,&V1,&n,&ref);
	}
	//if(rank==0) printit(T1,m,V1,n,rank);*/

	//circle mesh//
	/*circle_info info;
	info.center_x=0.0;
	info.center_y=0.0;
	info.maximum_triangle_area=0.1;
	info.radius=1;
	info.min_angle=30;
	info.number_of_points=10;
	circular_mesh(&T1,&m,&V1,&n,info);
	if(rank==0) printit(T1,m,V1,n,0);*/
	//uniform(&T1,&m,&V1,&n,&ref);
	//uniform(&T1,&m,&V1,&n,&ref);
	//uniform(&T1,&m,&V1,&n,&ref);
	//uniform(&T1,&m,&V1,&n,&ref);
	//Square mesh//
	/*square_info infos;
	infos.min_angle=30;
	infos.maximum_triangle_area=0.05;
	double corner[]={0.0,0.0,1.0,0.0,1.0,1.0,0.0,1.0};
	int segments[]={0,1,1,2,2,3,3,0};
	memcpy(&infos.corners, &corner, sizeof(corner));
	memcpy(&infos.segments,&segments,sizeof(segments));
	square_mesh(&T1,&m,&V1,&n,infos);
	//uniform(&T1,&m,&V1,&n,&ref);
	printit(T1,m,V1,n,1);*/


	double *mesh_quality;
	int *globaloffset,*boundary_edge_list,size_list;
	if(rank==0) Tri_mesh_quality(T1,m,V1,n,&mesh_quality);
	if(rank==0) Is_mesh_good(mesh_quality,m);
	Mesh_info_f(T1,n,m,&boundary,&boundary_edge_list,&size_list);

	offset(T1,m,V1,n,&globaloffset,
			boundary,NEdges,boundary_edge_list,size_list);
	int * Tseq,*Tseq2,m1,n1,m2;
	double *Vseq;
	int proc=rank,neibours[20],size_of_neib,*master_offset;;
	int *cumsum;

	Sequenctial_Triangle(&T1[3 * proc], V1, n, NEdges, &Tseq, &m1, &Vseq, &n1);//Local mesh
	Cumsumfunc(globaloffset,&cumsum,m);
	//printit(Tseq,m1,Vseq,n1,proc+1);

	set_Global_offset(T1,m,n1,&master_offset,NEdges,cumsum[proc],boundary,proc,boundary_edge_list,size_list,cumsum);

	//if(proc==0) for(i=0;i<n1;i++){ printf("gf: %d\n",master_offset[i]);}
	double *Global_stiff,*gfval;
	int sizeG,*Gcol,*Grow,*gfr,sizeF,sizebound,*renumber_verts;
	fix_triangle_orientation(Tseq,m1,Vseq,n1);
	Globall_stiffness_Mat(&Global_stiff, &sizeG, &Gcol, &Grow, Tseq, m1, Vseq, n1,
			&gfval, &gfr, &sizeF);

	find_sizeof_boundary(NEdges, master_offset, &sizebound);
	Renumber_Verts(master_offset, n1, &renumber_verts);

	*PMAT_Stiff = Global_stiff;
	*sizePMAT = sizeG;
	*Global_columns = Gcol;
	*Global_rows = Grow;
	*gfvalues = gfval;
	*gfrows = gfr;
	*sizef = sizeF;
	*Goffset = master_offset;
	*sizeV = n1;
	*sizeofbound = sizebound;
	*renumber=renumber_verts;

	free(globaloffset);
	free(cumsum);
	free(T1);
	free(V1);
	free(Vseq);
	free(Tseq);
	free(boundary);


	return 0;

}


/****** Assemble Local Neumman Problems*********/
extern PetscErrorCode Assemble_Neumman(int rank,int subdivisions, int NEdges,\
		int**locanumberf,int **globalumberf,int *sizeALocf,Mat *Af,Vec *fi){
	int i,sizeALoc,count=0,proc;
	double *PMAT_Stiff,* gfvalues;
	int sizePMAT,* Global_columns,* Global_rows,* gfrows,sizef;
	int sizebound,*GlobalOffset,sizeV;
	int* renumber;
	/**PETSC VARIABLES**/
	Mat  A;
	Vec  f;
	PetscErrorCode ierr;
	PetscScalar  ValueA;
	PetscInt row,column;


	proc=rank;
	//printf("rank %d",rank);
	Proc_Stiffnes_Mat(proc,NEdges,subdivisions,\
			&PMAT_Stiff,&sizePMAT,&Global_columns,&Global_rows,
			&gfvalues,&gfrows,&sizef,&GlobalOffset,&sizebound,&sizeV,&renumber);
	sizeALoc=sizeV-sizebound;

	int *cnt=calloc(sizeALoc,sizeof(int));
	for(i=0;i<sizePMAT;i++){
		        if(renumber[Global_rows[i]]!=-1){
		        //printf("%d \n",Global_rows[i]);
				cnt[renumber[Global_rows[i]]]=cnt[renumber[Global_rows[i]]]+1;}
	}

	for(i=0;i<sizeALoc;i++) { cnt[i]=(int)floor(cnt[i]/2)+3; if(cnt[i]>=sizeALoc) {cnt[i]=sizeALoc;}}

	//puts("");
	MatCreateSeqAIJ(PETSC_COMM_SELF,sizeALoc,sizeALoc,0,cnt,&A);
	MatSetOption(A,MAT_IGNORE_ZERO_ENTRIES,PETSC_TRUE);
	MatSetOption(A,MAT_SPD,PETSC_TRUE);
    MatSetUp(A);
	for(i=0;i<sizePMAT;i++){
		row=renumber[Global_rows[i]];
		column=renumber[Global_columns[i]];
		ValueA=PMAT_Stiff[i];
		if((row>-1) && (column>-1) && (ValueA!=0)){
		MatSetValues(A,1,&column,1,&row,&ValueA,ADD_VALUES);
		}
	}
	MatAssemblyBegin(A,MAT_FINAL_ASSEMBLY);
	MatAssemblyEnd(A,MAT_FINAL_ASSEMBLY);

	//MatInfo info;
	//double mem;
	//MatGetInfo(A,MAT_LOCAL,&info);
	//int nz_a = (int)info.nz_allocated;
	//int nz_u = (int)info.nz_used;
	//mem=info.memory;
	//printf("%d %d %f\n",nz_a,nz_u,mem);
	//PetscViewerASCIIOpen(PETSC_COMM_SELF, "Dmat.m", &viewer);
    //PetscViewerSetFormat( viewer, PETSC_VIEWER_ASCII_MATLAB);
	//MatView(A,viewer);
	 //if(rank==2) MatView(A,PETSC_VIEWER_STDOUT_SELF);

	free(PMAT_Stiff);
	free(Global_rows);
	free(Global_columns);


	 VecCreate(PETSC_COMM_SELF,&f);
	 VecSetSizes(f,PETSC_DECIDE,sizeV-sizebound);
	 VecSetFromOptions(f);
	 VecZeroEntries(f);
	 for (i=0; i<sizef; i++) {
	  row=renumber[gfrows[i]];
	  ValueA= gfvalues[i];
	  //printf("%f\n",gfvalues[i]);
	  if(row>-1)
	  VecSetValues(f,1,&row,&ValueA,ADD_VALUES);
	  }

	 VecAssemblyBegin(f);
	 VecAssemblyEnd(f);
	//if(rank==2) VecView(f,PETSC_VIEWER_STDOUT_SELF);
	free(gfrows);
	free(gfvalues);
	//PetscViewerASCIIOpen(PETSC_COMM_SELF, "Dmat.m", &viewer);
	//PetscViewerSetFormat( viewer, PETSC_VIEWER_ASCII_MATLAB);
	//MatView(A,viewer);
	//MatView(A,PETSC_VIEWER_STDOUT_SELF);
	int *localnumber =malloc(sizeof(int)*sizeALoc+1);
	int *globalumber=malloc(sizeof(int)*sizeALoc+1);
	for(i=0;i<sizeV;i++){
		if(renumber[i]>-1){
			localnumber[count]=renumber[i];
			count=count+1;
		}
	}
	count=0;
	for(i=0;i<sizeV;i++){
		if(GlobalOffset[i]>-1){
			globalumber[count]=GlobalOffset[i];
			count=count+1;
		}
	}
	free(GlobalOffset);
	free(renumber);
	*locanumberf=localnumber;
	*globalumberf=globalumber;
	*sizeALocf=sizeALoc;
	 *Af=A;
	 *fi=f;
	 return(0);
}


/************* A function that assembles the MATIS MATRIX ***********************
 * input :
 * 			rank: rank of the process.
 * 			subdivisions: how man time we subdivide the mesh.
 * 			NEdges: number of vertices pers edge of the locla mesh.
 *	output:
 *			M: The MATIS MATRIX
 */
extern PetscErrorCode Assemble_MATIS(Mat A,int* localnumber,int *globalumber,\
		int sizeALoc,Mat *Mf){
	Mat M;
	ISLocalToGlobalMapping ltog;
	//PetscErrorCode ierr;
	/******** Start the procedure for the Matis Matrix **************/

	ISLocalToGlobalMappingCreate(PETSC_COMM_WORLD,sizeALoc,globalumber,PETSC_COPY_VALUES,&ltog);
	MatCreateIS(MPI_COMM_WORLD,1,sizeALoc,sizeALoc,PETSC_DETERMINE,PETSC_DETERMINE,ltog,&M);
	MatSetLocalToGlobalMapping(A,ltog,ltog);
	MatISSetLocalMat(M,A);
	MatAssemblyBegin(M,MAT_FINAL_ASSEMBLY);
	MatAssemblyEnd(M,MAT_FINAL_ASSEMBLY);
	ISLocalToGlobalMappingDestroy(&ltog);
	MatDestroy(&A);

	free(localnumber);
	free(globalumber);
    *Mf=M;
    return (0);
}



/* NOT TESTET FULL YET  */
/******************** Find_IS_Interface ***************************/
extern PetscErrorCode Find_IS_Interface(Mat M,int *sumf,\
		int **interfacef,int**interfacelocf,int*count_interfacef,\
		Vec *wf,int**interiorlocf,int* count_interiorf){
	Mat C;
	Vec x,y,w;
	IS is1;
	PetscInt n,*interface,*interfaceloc,count_interface,i,*interiorloc;//step=1,
	ISLocalToGlobalMapping ltog;
	PetscErrorCode ierr;
	PetscScalar  one=1.0,zero=0.0, sumt;
	int	sum=0;
	PetscScalar *w_array;
	PetscInt *globalnumbering,*localnumbering;
	MatISGetLocalMat(M,&C); //Get the local matrix
	MatGetLocalToGlobalMapping(C,&ltog,&ltog); //get the global to local indices
	ISLocalToGlobalMappingGetSize(ltog,&n);

	ierr = VecCreate(PETSC_COMM_WORLD,&x);CHKERRQ(ierr);
	ierr = VecSetSizes(x,n,PETSC_DECIDE);CHKERRQ(ierr);
	ierr = VecSetFromOptions(x);CHKERRQ(ierr);
	//ierr = VecSet(x,zero);CHKERRQ(ierr);
    ierr = VecSetLocalToGlobalMapping(x,ltog);CHKERRQ(ierr);
	for (i=0; i<n; i++) {
		ierr = VecSetValuesLocal(x,1,&i,&one,INSERT_VALUES);CHKERRQ(ierr);
	}
	ierr = VecAssemblyBegin(x);CHKERRQ(ierr);
	ierr = VecAssemblyEnd(x);CHKERRQ(ierr);
	VecSum(x,&sumt);
	VecDestroy(&x);
	sum=sumt;
	ierr = VecCreate(PETSC_COMM_WORLD,&x);CHKERRQ(ierr);
	ierr = VecSetSizes(x,PETSC_DECIDE,sum);CHKERRQ(ierr);
	ierr = VecSetFromOptions(x);CHKERRQ(ierr);
	ierr = VecSet(x,zero);CHKERRQ(ierr);
	ierr = VecSetLocalToGlobalMapping(x,ltog);CHKERRQ(ierr);
	for (i=0; i<n; i++) {
		ierr = VecSetValuesLocal(x,1,&i,&one,ADD_VALUES);CHKERRQ(ierr);
	}
	/******* We Assemble x and duplicate x to w **********/
	ierr = VecDuplicate(x,&w);CHKERRQ(ierr);
	ierr = VecAssemblyBegin(x);CHKERRQ(ierr);
	ierr = VecAssemblyEnd(x);CHKERRQ(ierr);
	/********   The vector W and we make it local ********/
	ierr = VecSet(w,one);CHKERRQ(ierr);CHKERRQ(ierr);
	ierr = VecPointwiseDivide(w,w,x);CHKERRQ(ierr);


	/***************************************************/
	PetscMalloc(n*sizeof(PetscInt),&globalnumbering);
	PetscMalloc(n*sizeof(PetscInt),&localnumbering);
	PetscMalloc(n*sizeof(PetscScalar),&w_array);
	for(i=0;i<n;i++){
		localnumbering[i]=i;
	}
	ISLocalToGlobalMappingApply(ltog,n,localnumbering,globalnumbering);
	ISCreateGeneral(MPI_COMM_SELF,n,globalnumbering,PETSC_COPY_VALUES,&is1);
	VecGetSubVector(w,is1,&y);
	VecGetArray(y,&w_array);

	count_interface=0;
	for(i=0;i<n;i++){
		if(w_array[i]<1){
			count_interface=count_interface+1;
		}
	}
	/***************Interface Vertices*****************/
	PetscMalloc(count_interface*sizeof(PetscInt),&interface);
	PetscMalloc(count_interface*sizeof(PetscInt),&interfaceloc);
	PetscMalloc((n-count_interface)*sizeof(PetscInt),&interiorloc);
	count_interface=0;
	int count_interior=0;
	for(i=0;i<n;i++){
		if(w_array[i]<1){
			interface[count_interface]=globalnumbering[i];
			interfaceloc[count_interface]=i;
			count_interface=count_interface+1;
		}else{
			interiorloc[count_interior]=i;
			count_interior=count_interior+1;
		}
	}
	/*print_array_int(interfaceloc,count_interface,1); //TO REMOVE;
	print_array_int(interiorloc,count_interior,2);*/
	PetscFree(localnumbering);
	PetscFree(globalnumbering);
	VecRestoreArray(y,&w_array);
	VecRestoreSubVector(w,is1,&y);
	VecDestroy(&y);
	VecDestroy(&x);
	ISDestroy(&is1);
	PetscFree(w_array);
	VecDestroy(&x);
	ISLocalToGlobalMappingDestroy(&ltog);



	*wf=w;
	*count_interfacef=count_interface;
	*interfacef=interface;
	*interfacelocf=interfaceloc;
	*interiorlocf=interiorloc;
	*count_interiorf=count_interior;
	*sumf=sum;

	return(0);
}

/********* Assemble K MATRIX***************************
 * K orthogonal Projection Matrix.
 * input: MATIS Matrix
 *
 * output: K Shell Matrix
 */
/********* A funciton that Assembles K exmplicitly *********/
extern PetscErrorCode Assemble_KE(PetscInt sum,int* interface,		\
				  int count_interface,Mat *Kf,int rank,int size,Vec w){
  Mat G,GT,W,K,GW;

  int flag=1;
  int i,rowStartG,rowEndG,row,col,count,rowStartW,rowEndW;
  PetscScalar one=1.0,val;


  MatCreate(PETSC_COMM_WORLD,&G);
  MatSetSizes(G,count_interface,PETSC_DECIDE,PETSC_DETERMINE,sum);
  MatSetType(G,MATAIJ);
  MatSetUp(G);
	//MatMPIAIJSetPreallocation(G,1,NULL,1,NULL);
  MatSetFromOptions(G);
  MatGetOwnershipRange(G,&rowStartG,&rowEndG);


  count=0;
  for(i=rowStartG;i<rowEndG;i++){
    col=interface[count];
    count=count+1;
    MatSetValues(G,1,&i,1,&col,&one,INSERT_VALUES);
  }


  MatAssemblyBegin(G,MAT_FINAL_ASSEMBLY);
  MatAssemblyEnd(G,MAT_FINAL_ASSEMBLY);


  MatCreate(PETSC_COMM_WORLD,&W);
  MatSetSizes(W,PETSC_DECIDE,PETSC_DECIDE,sum,sum);
  MatSetType(W,MATAIJ);
  //MatSetUp(W);
  MatMPIAIJSetPreallocation(W,1,NULL,1,NULL);
  MatSetFromOptions(W);
  MatGetOwnershipRange(W,&rowStartW,&rowEndW);

  for(i=rowStartW;i<rowEndW;i++){
    val=0.0;
    MatSetValues(W,1,&i,1,&i,&val,INSERT_VALUES);
  }

  MatAssemblyBegin(W,MAT_FINAL_ASSEMBLY);
  MatAssemblyEnd(W,MAT_FINAL_ASSEMBLY);
  MatDiagonalSet(W,w, INSERT_VALUES);
  VecDestroy(&w);

  MatCreate(PETSC_COMM_WORLD,&GT);
  MatSetSizes(GT,PETSC_DECIDE,count_interface,sum,PETSC_DETERMINE);
  MatSetType(GT,MATAIJ);
  MatSetUp(GT);
  //MatMPIAIJSetPreallocation(G,1,NULL,1,NULL);
  MatSetFromOptions(GT);

  int j;
  //MatSetOption(GT,MAT_ROW_ORIENTED,PETSC_FALSE);
  MatGetOwnershipRangeColumn(GT,&rowStartG,&rowEndG);
  //printf(" %d %d %d,%d\n",rowStartG,rowEndG,rank,cumsum[rank]);
  count=0;
  for(j=rowStartG;j<rowEndG;j++){
    row=interface[count];
    col=j;
    MatSetValues(GT,1,&row,1,&col,&one,INSERT_VALUES);
    count=count+1;
  }

  MatAssemblyBegin(GT,MAT_FINAL_ASSEMBLY);
  MatAssemblyEnd(GT,MAT_FINAL_ASSEMBLY);

  //MatView(GT,PETSC_VIEWER_STDOUT_WORLD);
  MatMatMult(G,W,MAT_INITIAL_MATRIX,PETSC_DEFAULT,&GW);
  MatDestroy(&G);
  MatDestroy(&W);
  MatMatMult(GW,GT,MAT_INITIAL_MATRIX,PETSC_DEFAULT,&K);
  //MatView(K,PETSC_VIEWER_STDOUT_WORLD);
  MatDestroy(&GW);
  MatDestroy(&GT);
  *Kf=K;
  return 0;
}

typedef struct{
	PetscScalar Robin_parin;
	Vec temp,z;
	//IS is;
	int *interfaceloc;
	int sizeinterface;
	KSP solver;
}QLUdata;


extern int QLUmult(Mat A,Vec lambda,Vec Qlambda){

	QLUdata *data;
	//int coutit;
	PetscScalar *values,val,*values2;
	int i,m,Rlow,Rend;
	VecGetOwnershipRange(lambda,&Rlow,&Rend);
	MatShellGetContext(A,&data);
	//PetscMalloc(data->sizeinterface*sizeof(PetscScalar),&values);
	VecGetArray(lambda,&values);
	for(i=0;i<(data->sizeinterface);i++){
		val=values[i];
		m=data->interfaceloc[i];
		VecSetValues(data->temp,1,&m,&val,INSERT_VALUES);
	}
	VecRestoreArray(lambda,&values);
	PetscFree(values);
	VecAssemblyBegin(data->temp);
	VecAssemblyEnd(data->temp);

	KSPSolve(data->solver,data->temp,data->z);
	//VecView(data->z,PETSC_VIEWER_STDOUT_SELF);

	VecScale(data->z,data->Robin_parin);
	PetscMalloc(data->sizeinterface*sizeof(PetscScalar),&values2);
	VecGetArray(data->z,&values2);
	for(i=Rlow;i<Rend;i++){
	    m=data->interfaceloc[i-Rlow]+Rlow;
	    val=values2[i-Rlow];
	    VecSetValues(Qlambda,1,&m,&val,INSERT_VALUES);
	  }
	  VecAssemblyBegin(Qlambda);
	  VecAssemblyEnd(Qlambda);
	  VecRestoreArray(data->z,&values2);

	return 0;
}




extern PetscErrorCode Assemble_QLU(Mat M,int* interface,int* interfaceloc,\
								int count_interface,PetscScalar Robin_par,Mat *Qf,KSP* solverf){
	Mat Q,RobI;
	int i,m,n;
	Mat FC;
	//IS  is;
	Vec temp,z;
	PetscInt nz=10,col;
	PetscScalar one=1.0;//,zeros=0.0;
	KSP  solver;
	PC pc;
	QLUdata* datainQ;


	MatISGetLocalMat(M,&FC); //Get the local matrix

	MatGetSize(FC,&m,&n);
	VecCreateSeq(PETSC_COMM_SELF,n,&temp);

	VecDuplicate(temp,&z);


	MatCreate(PETSC_COMM_SELF,&RobI);
	MatSetSizes(RobI,m,n,m,n);
	MatSetType(RobI,MATSEQAIJ);
	MatSeqAIJSetPreallocation(RobI,nz,NULL);
	//MatSetUp(RobI);

	for(i=0;i<count_interface;i++){
		col=interfaceloc[i];
	 MatSetValues(RobI,1,&col,1,&col,&Robin_par,INSERT_VALUES);
	}
	MatAssemblyBegin(RobI,MAT_FINAL_ASSEMBLY);
	MatAssemblyEnd(RobI,MAT_FINAL_ASSEMBLY );
	//MatAXPY(FC,one,RobI,DIFFERENT_NONZERO_PATTERN);
	MatAXPY(RobI,one,FC,DIFFERENT_NONZERO_PATTERN);
	//MatView(FC,PETSC_VIEWER_STDOUT_SELF);
	MatDestroy(&FC);


	 KSPCreate(PETSC_COMM_SELF,&solver);
	 KSPSetType(solver, KSPPREONLY);
	 //KSPSetType(solver,KSPGMRES);
	 KSPGetPC(solver,&pc);
	 //PCSetType(pc,PCCHOLESKY);
	 PCSetType(pc,PCLU);
	 KSPSetOperators(solver,RobI,RobI,SAME_PRECONDITIONER);
	 KSPSetUp(solver);

	PetscMalloc(sizeof(QLUdata),&datainQ);
	datainQ->Robin_parin=Robin_par;
	datainQ->interfaceloc=interfaceloc;
	datainQ->sizeinterface=count_interface;
	datainQ->temp=temp;
	datainQ->z=z;
	//datainQ->is=is;
	datainQ->solver=solver;

	MatCreateShell(PETSC_COMM_WORLD,count_interface,count_interface,PETSC_DETERMINE,\
			PETSC_DETERMINE,datainQ,&Q);
	MatShellSetOperation(Q,MATOP_MULT,(void(*)(void))QLUmult);
	MatSetFromOptions(Q);
	MatDestroy(&RobI);
	*Qf=Q;
	*solverf=solver;

	return 0;
}

/********************** PRECONDITIONER ******************************************/

static PetscErrorCode LExpicit(Mat *Lf,Mat J,Mat JT,Mat K,int count_interface,int size){
  Mat C,Ident,KJ;
	int rowStart,rowEnd,i;
	PetscScalar minusone=-1.0,one=1.0;

	MatCreate(PETSC_COMM_WORLD,&Ident);
	MatSetSizes(Ident,PETSC_DECIDE,PETSC_DECIDE,size,size);
	MatSetUp(Ident);
	MatSetFromOptions(Ident);
	MatGetOwnershipRange(Ident, &rowStart,&rowEnd);

	for (i=rowStart; i<rowEnd; i++){
		MatSetValue(Ident,i,i,one,INSERT_VALUES);
	}
	MatAssemblyBegin(Ident,MAT_FINAL_ASSEMBLY);
	MatAssemblyEnd(Ident,MAT_FINAL_ASSEMBLY);

	//MatMatMatMult(JT,K,J,MAT_INITIAL_MATRIX,PETSC_DEFAULT,&C);
	MatMatMult(K,J,MAT_INITIAL_MATRIX,PETSC_DEFAULT,&KJ);
	MatMatMult(JT,KJ,MAT_INITIAL_MATRIX,PETSC_DEFAULT,&C);
	MatAXPY(C,minusone,Ident,DIFFERENT_NONZERO_PATTERN);
	MatDestroy(&Ident);
	MatDestroy(&KJ);
	*Lf=C;
	return 0;
}

int MatParToSeq(Mat *OUTMAT,Mat INMAT,int size,int rank){
	//const PetscScalar *vals;
	//const PetscInt *cols;
	PetscInt rowStartK,rowEndK,Globalrows,Globalcolumns;//,r,j,ncols,,N
	Mat *Lseq,F;
	PetscErrorCode ierr;
	IS *is;

	ierr= MatGetSize(INMAT,&Globalrows,&Globalcolumns);CHKERRQ(ierr);
	ierr=MatGetOwnershipRange(INMAT,&rowStartK,&rowEndK);CHKERRQ(ierr);
	//MatView(INMAT,PETSC_VIEWER_STDOUT_WORLD);
	PetscMalloc(sizeof(IS **),&is);

	ISCreateStride(PETSC_COMM_WORLD,Globalcolumns,0,1,is);

	MatGetSubMatrices(INMAT,1,is,is,MAT_INITIAL_MATRIX,&Lseq);
	ISDestroy(is);
	PetscFree(is);
	MatDuplicate(Lseq[0], MAT_COPY_VALUES,&F);
	MatDestroyMatrices(1,&Lseq);
	*OUTMAT=F;
	return 0;
}



typedef struct{
	Mat J,JT;
	Vec TempJ,TempJT,z,z1;
	int count_interface,flag,Rlow,RlowJ,RendJ,size,rank;
	KSP solver;
}Precondata3;

extern PetscErrorCode Precon3_func(Mat P,Vec lambda, Vec y){

	Precondata3 *datain;
	PetscScalar minusone=-1.0,one=1.0,*avec,*bvec;
	Vec vout,f;//,temp;
	int i; // its,
	IS isscat;


	MatShellGetContext(P,&datain);
	ISCreateStride(PETSC_COMM_WORLD,datain->rank==0?datain->size:0,datain->rank==0?0:datain->size,1,&isscat);
    MatMult(datain->JT,lambda,datain->TempJ); //JTlamb course vector.
    VecGetSubVector(datain->TempJ,isscat,&vout);

    if(datain->rank==0){
       VecGetArray(vout,&avec);
       VecCreateSeqWithArray(PETSC_COMM_SELF,1,datain->size,avec,&f);
       KSPSolve(datain->solver,f,datain->z1);
       VecRestoreArray(vout,&avec);
       VecDestroy(&f);
    }
    VecRestoreSubVector(datain->TempJ,isscat,&vout);
    if(datain->rank==0){
        VecGetArray(datain->z1,&bvec);
        for(i=0;i<datain->size;i++){
           VecSetValue(datain->z,i,bvec[i],INSERT_VALUES);
        }
        VecRestoreArray(datain->z1,&bvec);
      }
      VecAssemblyBegin(datain->z);
      VecAssemblyEnd(datain->z);


    MatMult(datain->J,datain->z,datain->TempJT);
    MatMult(datain->J,datain->TempJ,y);
    VecAXPBYPCZ(y,one,minusone,minusone,lambda,datain->TempJT);
    ISDestroy(&isscat);
	return 0;
}

PetscErrorCode Precon3(Mat *Pf,Mat M,int count_interface,Mat K,int rank,int size){
	Mat J,JT,P,TempA,L,LE;
	Vec z,z1,out,TempJ,TempJT;
	int n,flag,rowStartJ,rowEndJ,i,cumsum=0;//,rowId,colId;,m,
	PetscScalar norm,one=1.0,val;//,zero=0.0;
	Precondata3 *datain;
	KSP solver;
	PC pc;

	MatISGetLocalMat(M,&TempA);
	MatGetSize(TempA,&n,&n);
	VecCreate(PETSC_COMM_SELF,&z);
	VecSetSizes(z,n,n);
	VecSetFromOptions(z);
	VecSet(z,one);
	VecDuplicate(z,&out);
	MatMult(TempA,z,out);
	//VecView(out,PETSC_VIEWER_STDOUT_SELF);
	VecNorm(out,NORM_2,&norm);
	//if(norm<1e-12){ flag=1;
	if(norm<1e-6){ flag=1;
	}else{ flag=0;}
	//printf("%f\n",norm);
	int *totalcount=malloc(sizeof(int)*size);
	MPI_Allgather(&count_interface, 1, MPI_INT,totalcount,1,MPI_INT,
			PETSC_COMM_WORLD);
	if(rank>0){
	for(i=0;i<rank;i++){
		cumsum=cumsum+totalcount[i];
	}
	}
	//printf("RANK %d %d\n",rank,cumsum);
	VecDestroy(&z);
	VecDestroy(&out);

	MatCreate(PETSC_COMM_WORLD,&J);
	MatSetSizes(J,count_interface,1,PETSC_DETERMINE,PETSC_DETERMINE);
	MatSetType(J,MATMPIAIJ);
	MatMPIAIJSetPreallocation(J,1,NULL,1,NULL);
	MatSetFromOptions(J);


	MatCreate(PETSC_COMM_WORLD,&JT);
	MatSetType(JT,MATMPIAIJ);
	MatSetSizes(JT,1,count_interface,PETSC_DETERMINE,PETSC_DETERMINE);
    MatSetUp(JT);


	MatGetOwnershipRange(J,&rowStartJ,&rowEndJ);

	if(flag){
	for(i=rowStartJ;i<rowEndJ;i++){
		val=1./sqrt(count_interface);
		MatSetValues(J,1,&i,1,&rank,&val,INSERT_VALUES);
	}
	}

	MatAssemblyBegin(J,MAT_FINAL_ASSEMBLY);
	MatAssemblyEnd(J,MAT_FINAL_ASSEMBLY);

	if(flag){
			for(i=cumsum;i<cumsum+count_interface;i++){
				val=1./sqrt(count_interface);
				MatSetValues(JT,1,&rank,1,&i,&val,INSERT_VALUES);
		}
	}
	MatAssemblyBegin(JT,MAT_FINAL_ASSEMBLY);
	MatAssemblyEnd(JT,MAT_FINAL_ASSEMBLY);



	VecCreate(PETSC_COMM_WORLD,&TempJ);
	VecSetSizes(TempJ,1,PETSC_DECIDE);
	VecSetFromOptions(TempJ);

	VecDuplicate(TempJ,&z);

	VecCreate(PETSC_COMM_SELF,&z1);
	VecSetSizes(z1,size,size);
	VecSetFromOptions(z1);



	VecCreate(PETSC_COMM_WORLD,&TempJT);
	VecSetSizes(TempJT,count_interface,PETSC_DECIDE);
	VecSetFromOptions(TempJT);



	LExpicit(&LE,J,JT,K,count_interface,size);
	MatParToSeq(&L,LE,size,rank);
	MatDestroy(&LE);


	KSPCreate(PETSC_COMM_SELF,&solver);
	KSPSetOperators(solver,L,L,SAME_PRECONDITIONER);
	KSPSetType(solver, KSPPREONLY);
	KSPGetPC(solver,&pc);
	//PCSetType(pc,PCCHOLESKY);
	PCSetType(pc,PCLU);
	KSPSetFromOptions(solver);
	KSPSetUp(solver);




	PetscMalloc(sizeof(Precondata3),&datain);
	datain->JT=JT;
	datain->J=J;
	datain->TempJ=TempJ;
	datain->TempJT=TempJT;
	datain->z=z;
	datain->z1=z1;
	datain->rank=rank;
	datain->size=size;
	datain->solver=solver;


	MatCreateShell(PETSC_COMM_WORLD,count_interface,count_interface,PETSC_DETERMINE,\
				PETSC_DETERMINE,datain,&P);
	MatShellSetOperation(P,MATOP_MULT,(void(*)(void))Precon3_func);
	MatSetFromOptions(P);
	MatDestroy(&L);

	 *Pf=P;

	return 0;
}

//*                   QMINK and IMIN2KQNINK                    *//
typedef struct{
	Mat K,Q;
	Vec z,ll,lm,lz;
}QminKdata;


extern PetscErrorCode QminKmul_func(Mat QminKmul,Vec x, Vec y){

	  QminKdata *QmK;
	  PetscScalar minusOne=-1.0;

	  /* need to get the context for K */
	  MatShellGetContext(QminKmul,&QmK);

	  MatMult(QmK->K,x,QmK->z);
	  MatMult(QmK->Q,x,y);
	  VecAXPY(y,minusOne,QmK->z);


	  return 0;

}
extern PetscErrorCode QMINK(Mat *QminKf,Mat K, Mat Q,int count_interface){

	QminKdata *datain;
	Mat QminK;
	Vec z;

	PetscMalloc(sizeof(QminKdata),&datain);

	datain->K=K;
	datain->Q=Q;
	VecCreate(PETSC_COMM_WORLD,&z);
	VecSetSizes(z,count_interface,PETSC_DECIDE);
    VecSetFromOptions(z);
    VecAssemblyBegin(z);
    VecAssemblyEnd(z);
    datain->z=z;
	MatCreateShell(PETSC_COMM_WORLD,count_interface,count_interface,PETSC_DETERMINE,\
				 	 	 	 	 	 	 	 	 	 	 	 PETSC_DETERMINE,datain,&QminK);
	MatShellSetOperation(QminK,MATOP_MULT,(void(*)(void))QminKmul_func);
	MatSetFromOptions(QminK);

	*QminKf=QminK;
	return 0;
}


extern PetscErrorCode IminKmulQminK_func(Mat IminKmulQminK,Vec x, Vec y){
  /* computes y = QminKmul *x */
  QminKdata *QmK;
  PetscScalar minusOne,minusTwo;

  MatShellGetContext(IminKmulQminK,&QmK);

  MatMult(QmK->K,x,QmK->ll);
  MatMult(QmK->Q,x,QmK->z);
  minusOne=-1.0;
  minusTwo=-2.0;

  VecWAXPY(QmK->lz,minusOne,QmK->ll,QmK->z);
  MatMult(QmK->K,QmK->lz,QmK->lm);

  VecCopy(QmK->lz,y);
  VecAXPY(y,minusTwo,QmK->lm);

  return 0;
}



extern PetscErrorCode IminKQMINK(Mat *IminQminKf,Mat K, Mat Q,int count_interface){

	QminKdata *datain;
	Mat QminK;
	Vec z;//lm,lz,ll;

	PetscMalloc(sizeof(QminKdata),&datain);

	datain->K=K;
	datain->Q=Q;
	VecCreate(PETSC_COMM_WORLD,&z);
	VecSetSizes(z,count_interface,PETSC_DECIDE);
    //PetscScalar zero=0.0;
    VecSetFromOptions(z);
    //VecSet(z,zero);
    VecDuplicate(z,&(datain->ll));
    VecDuplicate(z,&(datain->lz));
    VecDuplicate(z,&(datain->lm));
    datain->z=z;
	MatCreateShell(PETSC_COMM_WORLD,count_interface,count_interface,PETSC_DETERMINE,\
				 	 	 	 	 	 	 	 	 	 	 	 PETSC_DETERMINE,datain,&QminK);
	MatShellSetOperation(QminK,MATOP_MULT,(void(*)(void))IminKmulQminK_func);
	MatSetFromOptions(QminK);

	*IminQminKf=QminK;
	return 0;
}


int Solver_KSP(Mat EQMINK,Mat P,int count_interface,int rank,int flag,Vec rhs,Vec *lambdaf){
	Vec b,x;
	KSP solver;
	PC pc;
	PetscScalar one=1.0;
	int niter;

	//VecCreate(PETSC_COMM_WORLD,&b);
	//VecSetSizes(b,count_interface,PETSC_DECIDE);
	//VecSetFromOptions(b);
	//VecSet(b,one);
	//VecDuplicate(b,&x);
	VecDuplicate(rhs,&x);
	/* This method explicitly requires GMRES */
	KSPCreate(PETSC_COMM_WORLD,&solver);
	KSPSetType(solver,KSPGMRES);
	KSPSetInitialGuessNonzero(solver,PETSC_TRUE);
    KSPGetPC(solver,&pc);
	PCSetType(pc,PCMAT);



	KSPSetOperators(solver,EQMINK,P,DIFFERENT_NONZERO_PATTERN);
	KSPSetTolerances(solver, 1e-7,  /* relative convergence tolerance */
			1e-6,      /* absolute convergence tolerance */
			PETSC_DEFAULT, /* divergence tolerance */
			50000);    /* max number of iterations */

	/* overide any of the above default options, suck them from the command line */

	KSPSetFromOptions(solver);

	/* do it, do it Verne */
	KSPSolve(solver,rhs,x);
	//VecView(x,PETSC_VIEWER_STDOUT_WORLD);

	/* How did we do ? */
	KSPGetIterationNumber(solver,&niter);
	if(rank==0){
		puts("======== Finished with Outer Solve ========");
	    printf("Iterations %d\n",niter);
	}
	//KSPDestroy(&solver);
	//VecDestroy(&b);
	*lambdaf=x;
	//VecDestroy(&x);

	return 0;

}


int G_Vector(Mat M,Vec *v,Vec f,int* interface,int sizeinterface,int* interior,int size_interior){
	Mat TempA,*AII,*AGI;
	Vec fI,fG,z,l;
	IS *isI,*isG;
	KSP solver;
	PC pc;
	PetscScalar minusone=-1.0;

	MatISGetLocalMat(M,&TempA);
	//print_array_int(interface,sizeinterface,1);
	PetscMalloc(sizeof(IS *),&isI);
	PetscMalloc(sizeof(IS *),&isG);
	ISCreateGeneral(PETSC_COMM_SELF,size_interior,interior,PETSC_COPY_VALUES,isI);
	ISCreateGeneral(PETSC_COMM_SELF,sizeinterface,interface,PETSC_COPY_VALUES,isG);
	//ISView(*isI,PETSC_VIEWER_STDOUT_SELF);

	MatGetSubMatrices(TempA,1,isI,isI,MAT_INITIAL_MATRIX,&AII);
	MatGetSubMatrices(TempA,1,isG,isI,MAT_INITIAL_MATRIX,&AGI);
	VecGetSubVector(f,isI[0],&fI);
	VecGetSubVector(f,isG[0],&fG);

	//MatView(AGI[0],PETSC_VIEWER_STDOUT_SELF);
	//VecView(fG,PETSC_VIEWER_STDOUT_SELF);

	KSPCreate(PETSC_COMM_SELF,&solver);
	KSPSetType(solver, KSPPREONLY);
	KSPGetPC(solver,&pc);
	//PCSetType(pc,PCCHOLESKY);
	PCSetType(pc,PCLU);
	KSPSetOperators(solver,AII[0],AII[0],DIFFERENT_NONZERO_PATTERN);
	KSPSetUp(solver);
	VecDuplicate(fI,&z);
	VecDuplicate(fG,&l);
	KSPSolve(solver,fI,z);
	//VecView(z,PETSC_VIEWER_STDOUT_SELF);
	MatMult(AGI[0],z,l);

	VecAXPY(l,minusone,fG);
	//VecView(l,PETSC_VIEWER_STDOUT_SELF);
	VecRestoreSubVector(f,isI[0],&fI);
    VecRestoreSubVector(f,isG[0],&fG);
	ISDestroy(isG);
    PetscFree(isG);
    ISDestroy(isI);
    PetscFree(isI);
	*v=l;
	VecDestroy(&z);
	VecDestroy(&fG);
	VecDestroy(&fI);
	MatDestroy(AII);
	MatDestroy(AGI);
    return 0;
}


int RHSSymetric(Vec *rhsf,Mat Q,Vec g,int count_interface){
	Vec rhs,temp;
	int start,end;
	int i,count=0,j;//,endtemp;
	PetscScalar v,*array;
	VecCreate(PETSC_COMM_WORLD,&temp);
    VecSetSizes(temp,count_interface,PETSC_DECIDE);
    VecSetFromOptions(temp);
    VecGetOwnershipRange(temp,&start,&end);
    //VecView(g,PETSC_VIEWER_STDOUT_SELF);
    VecGetArray(g,&array);

    for (i=0; i<(end-start); i++) {
         v = array[i];
         j=start+i;
         //printf("array %f\n",v);
         VecSetValues(temp,1,&j,&v,INSERT_VALUES);
         count=count+1;
    }
    VecAssemblyBegin(temp);
    VecAssemblyBegin(temp);
    VecDuplicate(temp,&rhs);
    //VecView(temp,PETSC_VIEWER_STDOUT_WORLD);

    VecRestoreArray(g,&array);
	MatMult(Q,temp,rhs);
	//VecView(rhs,PETSC_VIEWER_STDOUT_WORLD);
	VecDestroy(&g);
	VecDestroy(&temp);
	*rhsf=rhs;
	return 0;
}


int RecoverSulution(KSP solver,Vec f,Vec l,Vec* solution,int* interface,int size_intinterface){
	Vec temp;
	PetscScalar *lambdarray,one=1.0;
	VecDuplicate(f,&temp);
	VecZeroEntries(temp);
	VecGetArray(l,&lambdarray);
    VecSetValues(temp,size_intinterface,interface,lambdarray, INSERT_VALUES);
    VecAYPX(f,one,temp);
	KSPSolve(solver,f,temp);
	VecView(temp,PETSC_VIEWER_STDOUT_SELF);
	VecDestroy(&l);
	VecDestroy(&f);
	return 0;
}

int RHSNONSymetric(Vec *rhsf,Mat Q,Mat K,Vec g,int count_interface){
	Vec rhs,temp,tempK;
	int start,end;
	int i,count=0,j;//,endtemp;
	PetscScalar v,*array,minustwo=-2.0;
	VecCreate(PETSC_COMM_WORLD,&temp);
    VecSetSizes(temp,count_interface,PETSC_DECIDE);
    VecSetFromOptions(temp);
    VecGetOwnershipRange(temp,&start,&end);
    //VecView(g,PETSC_VIEWER_STDOUT_SELF);
    VecGetArray(g,&array);

    for (i=0; i<(end-start); i++) {
         v = array[i];
         j=start+i;
         //printf("array %f\n",v);
         VecSetValues(temp,1,&j,&v,INSERT_VALUES);
         count=count+1;
    }
    VecAssemblyBegin(temp);
    VecAssemblyBegin(temp);
    VecDuplicate(temp,&rhs);
    VecDuplicate(temp,&tempK);
    //VecView(temp,PETSC_VIEWER_STDOUT_WORLD);

    VecRestoreArray(g,&array);
	MatMult(Q,temp,rhs);
	MatMult(K,rhs,tempK);
	VecAXPY(rhs,minustwo,tempK);

	VecDestroy(&tempK);
	VecDestroy(&temp);
	VecDestroy(&g);
	//VecView(rhs,PETSC_VIEWER_STDOUT_WORLD);
	*rhsf=rhs;
	return 0;
}

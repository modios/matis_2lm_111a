% %Anastasios Karangelis 20 Junuary 2014. Some test for 2 Lagrange 
% %multiplier method.
% 
% %% Give T0 V0 the initial coarse mesh and Number of nodes pers edge
% 
% %data1
% %%
% node=V0;
% elem=T0+1;
% triplot(elem,node(:,1),node(:,2));
% n=size(V0,1);
% A1=stiff2d(node',elem');
% [M]=MassMat2D(node',elem');
% full(M)
% b=M*ones(size(M,1),1);
% b=LoadVec2D(node',elem',@Foo);
% A=full(A1);
% %% Writes in a txt file A matrix in order to read by C code;
% fileID = fopen('A0.txt','w');
% fprintf(fileID,'A[%d][%d]',n,n);
% fprintf(fileID,'{');
% for i=1:n
%  fprintf(fileID,'{');
%  for j=1:n-1
%   fprintf(fileID,'%f,',A(i,j));
%  end
%  fprintf(fileID,'%f',A(i,n));
%  fprintf(fileID,'},\n');
% end
% fprintf(fileID,'};\n');
% 
% fileID = fopen('A0.txt','w');
% fprintf(fileID,'A%d[%d][%d]',0,n,n);
% fprintf(fileID,'{');
% for i=1:n
%  fprintf(fileID,'{');
%  for j=1:n-1
%   fprintf(fileID,'%f,',A(i,j));
%  end
%  fprintf(fileID,'%f',A(i,n));
%  fprintf(fileID,'},\n');
% end
% fprintf(fileID,'};\n');
% fprintf(fileID,'b%d={\n',0);
% for i=1:n-1
%     fprintf(fileID,'%f,\n',b(i));
% end
% fprintf(fileID,'%f\n',b(n));
% fprintf(fileID,'};');
% 
% %fclose(fileID);
% 
% %% boundary
% %%%INFO: GIVE THE NEdges values%
% T=T0+1;
% N=max(max(T));
% triplot(T,V0(:,1),V0(:,2))
% hold on
% m=size(T,1);
% x=V0(:,1); y=V0(:,2);
% %% Here are the edges:
% E = [];
% for k=1:size(T,1)
%     for i=1:3
%         j = mod(i,3)+1;
%         E = [E;T(k,i) T(k,j)];
%     end
% end
% B=zeros(N);
% E = [min(E(:,1),E(:,2)) max(E(:,1),E(:,2))];
% for i=1:size(E,1)
%     B(E(i,1),E(i,2))=B(E(i,1),E(i,2))+1;
% end
% boundary=[];
% Edgelist=[];
% a=[];
% for i=1:N
%     a=find(B(i,:)==1);
%     if(length(a)>0)
%         boundary=[boundary,i,a];
%         for j=1:length(a)
%             Edgelist=[Edgelist; i a(j)];
%         end
%     end
%     a=[];
% end
% boudnary=unique(boundary);
% 
% plot(x(boundary),y(boundary),'ro');
% hold on; triplot(T,x,y)
% %%  Is orientation counter clockwise
% ve2 = V0(T(:,1),:)-V0(T(:,3),:);
% ve3 = V0(T(:,2),:)-V0(T(:,1),:);
% area = -ve3(:,1).*ve2(:,2)+ve3(:,2).*ve2(:,1);
% idx = find(area<0);
% if(isempty(idx))
%     fprintf('Orientation of triangles correct OK\n');
% end
% 
% 
% %% print boundary
% m=length(boudnary);
%  fprintf(fileID,'boundary[%d]={;\n',m);
% for i=1:m-1
%     fprintf(fileID,'%i,\n',boudnary(i)-1);
% end
% fprintf(fileID,'%i\n',boudnary(m)-1);
% fprintf(fileID,'};');
% %% Check Globla offset
% %INFO: GIVE THE NEdges values%
% NEdges=3; 
% % The number of vertices
% N = max(max(T));
% % This labels each vertex according to which triangle owns it
% LV = zeros(N,1);
% triplot(T,V0(:,1),V0(:,2))
% hold on
% %%
% for i=size(T,1):-1:1
%     for j=3:-1:1
%         LV(T(i,j)) = i;
%     end
% end
% % Here are the edges:
% E = [];
% for k=1:size(T,1)
%     for i=1:3
%         j = mod(i,3)+1;
%         E = [E;T(k,i) T(k,j)];
%     end
% end
% 
% E = [min(E(:,1),E(:,2)) max(E(:,1),E(:,2))];
% E1=E;
% [E,IE,IC] = unique(E,'rows');
% % Now we label each edge according to which triangle owns it
% LE = zeros(length(IE),1);
% for k=length(IC):-1:1
%     LE(IC(k),1) = ceil(k/3);
% end
% 
% %%
% 
% % Remove the label for boundary vertices
% LV(boundary) = 0;
% count=1;
% % Also remove the label for boundary edges
% num=size(Edgelist,1);
% flag=0;
% for k=1:size(LE,1)
%   if((E(k,1)==Edgelist(count,1)) && (E(k,2)==Edgelist(count,2)))
%       LE(k)=0;
%       if(count==num)
%        flag=1;   
%       end
%        count=count+1
%   end
%   if(flag==1) 
%       break;
%    end
% end
% 
% %%
% % Now we can compute K. We need to know how many interior and edge vertices
% % there are. This information comes from how many times we will further
% % subdivide. I make it up here.
% NI = NEdges*(NEdges-1)/2;
% NE = NEdges;
% M = size(T,1);
% K = repmat(NI,M,1);
% for k=1:length(LV)
%     if(LV(k)>0)
%         K(LV(k)) = K(LV(k)) + 1;
%     end
% end
% for k=1:length(LE)
%     if(LE(k)>0)
%         K(LE(k)) = K(LE(k)) + NE;
%     end
% end
% %% print GlobalOffset
% m=size(T,1);
%  fprintf(fileID,'Goff[%d]={\n',m);
% for i=1:m-1
%     fprintf(fileID,'%i,\n',K(i));
% end
% fprintf(fileID,'%i\n',K(m));
% fprintf(fileID,'};');
% 
% cumsum=zeros(m,1);
% cumsum(1)=0;
% for i=1:m-1
%    cumsum(1+i)=K(i)+cumsum(i);
% end
% 
%  fprintf(fileID,'cumsum[%d]={\n',m);
% for i=1:m-1
%     fprintf(fileID,'%i,\n',cumsum(i));
% end
% fprintf(fileID,'%i\n',cumsum(m));
% fprintf(fileID,'};');

%% Set GLoblal ofset

clear all
%Mesh128_3;  %%%% CHANGE THAT
Mesh128_3
T0=T0+1;
NE=3; %%%% CHANGE THAT
NI=NE*(NE-1)/2;
temp=V0==1 | V0==0;
temps2=sum(temp,2);
b=find(temps2==0);
triplot(T0,V0(:,1),V0(:,2),'k');
VT={};
box off
%% Here are the edges:


E = [];
N=max(max(T0));
x=V0(:,1);
y=V0(:,2);
for k=1:size(T0,1)
    for i=1:3
        j = mod(i,3)+1;
        E = [E;T0(k,i) T0(k,j)];
    end
end
B=zeros(N);
E = [min(E(:,1),E(:,2)) max(E(:,1),E(:,2))];
for i=1:size(E,1)
    B(E(i,1),E(i,2))=B(E(i,1),E(i,2))+1;
end
boundaryf=[];
Edgelist=[];
a=[];
for i=1:N
    a=find(B(i,:)==1);
    if(length(a)>0)
        boundaryf=[boundaryf,i,a];
        for j=1:length(a)
            Edgelist=[Edgelist; i a(j)];
        end
    end
    a=[];
end
boudnaryf=unique(boundaryf);
E1=Edgelist(:,1);
E2=Edgelist(:,2);
E1=[E1;Edgelist(:,2)];
E2=[E2;Edgelist(:,1)];
plot(x(boundaryf),y(boundaryf),'ro');
hold on; triplot(T0,x,y)

%%
figure(1)
for i=1:size(T0,1)
     eval(['V=V' num2str(i) ';']);
     VT{i}=V;
end
TF={};
for i=1:size(T0,1)
     eval(['T=T' num2str(i) ';']);
     TF{i}=T+1;
end
interior={};
boundary={};

for i=1:size(T0,1)
    a1=0; a2=0; a3=0;
    b1=0; b2=0; b3=0;
    boundary{i}=[];
 if (ismember(T0(i,1),boundaryf)) boundary{i}=[1]; end
 if (ismember(T0(i,2),boundaryf)) boundary{i}=[ boundary{i},2]; end
 if (ismember(T0(i,3),boundaryf)) boundary{i}=[ boundary{i},3]; end
 if(ismember(T0(i,1),boundaryf) && ismember(T0(i,2),boundaryf))
     a1=(T0(i,1)==E1); b1=(T0(i,2)==E2);
     if(sum(a1&b1))
       boundary{i}=[1,2];
       boundary{i}=[boundary{i},4:4+NE-1];  
     end
 end
 if(ismember(T0(i,2),boundaryf) && ismember(T0(i,3),boundaryf))
     a2=(T0(i,2)==E1); b2=(T0(i,3)==E2);
     if(sum(a2&b2))
     boundary{i}=[boundary{i},[2,3]];
     boundary{i}=[boundary{i},4+NE:4+2*NE-1];
     end
 end
  if(ismember(T0(i,3),boundaryf) && ismember(T0(i,1),boundaryf))
     a3=(T0(i,3)==E1); b3=(T0(i,1)==E2);
     if(sum(a3&b3)) 
     boundary{i}=[boundary{i},[3,1]];
     boundary{i}=[boundary{i},4+2*NE:4+3*NE-1];
     end
  end
  boundary{i}=unique(boundary{i})
  interior{i}=setdiff(1:3+3*NE+((NE-1)*NE)/2,boundary{i});
  plot(VT{i}(interior{i},1),VT{i}(interior{i},2),'ro');
  plot(VT{i}(boundary{i},1),VT{i}(boundary{i},2),'go');
end

% for i=1:size(T0,1);
%     C=VT{i}==0;
%     B=VT{i}==1;
%     CB=sum(B+C,2);
%     interior{i}=find(CB==0);
%     boundary{i}=find(CB~=0);
%     hold on;
%     plot(VT{i}(interior{i},1),VT{i}(interior{i},2),'ro');
%     plot(VT{i}(boundary{i},1),VT{i}(boundary{i},2),'go');
% end
% hold off
%%
T={};
Temp=[];
flag=0;
count=1;
for i=1:size(T0,1)
      a=ismember(TF{i}(:,1),boundary{i});
      b=ismember(TF{i}(:,2),boundary{i});
      c=ismember(TF{i}(:,3),boundary{i});
      d=a+b+c;
      temp=find(~d);
      T{i}=TF{i}(temp,:);
end

%%
V={};
ltgmap={}
for i=1:size(T0,1)
       ltgmap{i}=zeros(max(max(TF{i})),1);
       s=size(interior{i},1);
       ltgmap{i}(interior{i})=1:s;
end


for i=1:size(T0,1)
   V{i}=VT{i}(interior{i},:);
   hold on
   Temp=ltgmap{i}(T{i});
   triplot(Temp,V{i}(:,1),V{i}(:,2));
end
%%
VAll=vertcat(V{:});
VAll=unique(VAll,'rows');
figure(2)
plot(VAll(:,1),VAll(:,2),'ro');

local_to_global={};

for i=1:size(T0,1)
    a=size(V{i},1)-NI;
    temp=[];
    for j=1:a
        for m=1:size(VAll,1)
            if( V{i}(j,1)==VAll(m,1) && V{i}(j,2)==VAll(m,2))
             temp=[temp, m];
            end
        end
    end
    local_to_global{i}=temp;
end
  
verts=local_to_global;

C=horzcat(local_to_global{:});
xinter=unique(C);
mapping=zeros(length(xinter),1);

mapping(xinter)=1:length(xinter);
RG = {};
for i=1:size(T0,1)
 verts{i}=mapping(verts{i});
end

for k=1:size(T0,1)
 	    foo = verts{k};
        RG{k} = sparse(1:length(foo),foo,1,length(foo),length(xinter));
 end

 Rglobal = vertcat(RG{:});
 num = sum(Rglobal,1); 
 W = spdiags(1./num',0,length(num),length(num));
 K1 =Rglobal* W*(Rglobal');

%%

array=[];
for i=0:size(K1,1)-1
	array(i+1)=mod(i,2)+1;
end

F=K1*array';

%%
A={};
Aloc={};
AII={};
AGI={};
AIG={};
AGG={};
interfaceloc={};
triplot(T0,V0(:,1),V0(:,2));
A={};
c={};
sizeint=0;
interfacelocc={};
localiterc={};
for i=1:size(T0,1)
    A{i}=stiff2d(VT{i}',TF{i}');
    plot(VT{i}(interior{i},1),VT{i}(interior{i},2),'ro');
    interfaceloc=setdiff(1:size(VT{i},1)-NI,boundary{i});
    localiter=size(VT{i},1)-NI+1:size(VT{i},1);
    interfacelocc{i}=interfaceloc;
    localiterc{i}=localiter;
    Aloc{i}=A{i}(interior{i},interior{i});
    AII{i}=A{i}(localiter,localiter);
    AGG{i}=A{i}(interfaceloc,interfaceloc);
    AGI{i}=A{i}(interfaceloc,localiter);
    AIG{i}=A{i}(localiter,interfaceloc);
    sizeint=sizeint+size(interfaceloc,2);
    c{i} = LoadVec2D(VT{i}',TF{i}',@Foo);
end

%%
flags=zeros(size(T0,1),1);
for i=1:size(T0,1)
   if(norm(Aloc{i}*ones(size(Aloc{i},1),1))==0)
       flags(i)=1;
   end
end
a=find(flags);
for i=1:size(a,1)
    hold on
   triplot(TF{a(i)},VT{a(i)}(:,1),VT{a(i)}(:,2),'k');
end    
J=zeros(sizeint,size(T0,1));
countt=1;
for i=1:size(T0,1);
      interfaceloc=setdiff(1:size(VT{i},1)-NI,boundary{i}); 
      J(countt:countt+length(interfaceloc)-1,i)=ones(length(interfaceloc),1)./sqrt(length(interfaceloc));
      countt=countt+length(interfaceloc);
end
b=setdiff(1:size(T0,1),a);
sizeJ=size(J,1);
for i=1:size(b,2);
    J(:,b(i))=zeros(size(J,1),1);
end    

%%
S={};
Q={};
a=0.73;%%%%%%%%%%%%%% a
g={};
fI={};
fG={};
for i=1:size(T0,1)
    fI{i}=c{i}(localiterc{i});
    fG{i}=c{i}(interfacelocc{i});
    sizeint=sizeint+size(interfaceloc,2);
    c{i} = LoadVec2D(VT{i}',TF{i}',@Foo);
    S{i}=AGG{i}-AGI{i}*(AII{i}\AIG{i});
    Q{i}=a*inv(S{i}+a*eye(size(S{i})));
    g{i}=fG{i}-AGI{i}*(AII{i}\fI{i});
end


%% Q Matrix
Q=blkdiag(Q{:});
G=[];
G=vertcat(g{:});


QMINK2=(Q-K1);
IMIQK=(eye(size(K1))-2*K1)*QMINK2;

sol1=(QMINK2)\(-Q*G); %We find lambda for the symmetric case

%% We recover the solution of the problem for each subdomain.
count=0;
for i=1:size(T0,1)
    y1=[];x1=[];
    n=size(interfacelocc{i},2);
    m=size(localiterc{i},2);
    u=[AII{i} AIG{i}; AGI{i} (AGG{i}+a*eye(size(AGG{i})))]\([fI{i};fG{i}]...
                                +[zeros(m,1);sol1(count+1:count+n)]);
    count=count+n;
    u0 = zeros(length(c{i}),1);
    u0(interfacelocc{i},1) = u(end-n+1:end,1);
    u0(localiterc{i},1) = u(1:m,1);
    u0
    %scatter3(VT{i}(:,1),VT{i}(:,2),u0,'.')
    x1=(VT{i}(:,1));
    y1=(VT{i}(:,2));
    patch(x1(TF{i}'),y1(TF{i}'),u0(TF{i}'),i*ones(size(u0(TF{i}'))));
    %pdemesh(VT{i}',[],TF{i}',u0);
    hold on
end











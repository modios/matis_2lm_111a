// main.c Anastasios Karangelis-Sebastien Loisel

/*### MATIS_2LM 1.1.1a Solver #########
You can redistribute it and/or modify it under the terms of the GNU General Public License as published
 by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.
MATIS_2LM Solver is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without 
even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.You should have received a copy of the 
GNU General Public License along with this program. If not, see http://www.gnu.org/licenses/.
*/

static char help[]="MATIS_2LM solver 1.1.1b/Look at Readme.md for more details and examples";

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <math.h>
#include <assert.h>
#include <time.h>
#include "triangle.h"
#include "petsc.h"
#include "Functions2LM.h"


int main(int argc,char** argv){
	int NEdges=pow(2,2)-1; //Number of vertices per edge
	int subdivisions=3; //Number of subdivisions.

	int sizeALoc,precon=0;
	int *localnumber,*globalumber,sum,*interface,*interfaceloc,count_interface,count_interior,*interiorloc;
	PetscScalar Robin_par=0.73;
	PetscMPIInt rank,size;
	Mat M,K,Q,QminK,P;
	Mat A;
	Vec w,f,g,rhs;
	KSP solver;
	int n=1;
	Vec solution,lambda;


	PetscInitialize(&argc,&argv,(char *)0,help);
	MPI_Comm_rank(PETSC_COMM_WORLD,&rank);
	MPI_Comm_size(PETSC_COMM_WORLD,&size);
        PetscOptionsGetInt(NULL,"-n",&n,NULL);
	Robin_par=sqrt(1.0/(NEdges));
	//printf("My rank is %f\n",Robin_par);
	//if(precon==rank){
	//printf("My rank is %%d\n",rank);

	if(rank==0) printf("======= Assemble Neumman matix-started   ======== %d\n",rank);
	Assemble_Neumman(rank,subdivisions,NEdges,\
			&localnumber,&globalumber,&sizeALoc,&A,&f);
	if(rank==0) puts("======= Assemble Neumman matix-finished   ========\n");
	
	Assemble_MATIS(A,localnumber,globalumber,sizeALoc,&M);
	if(rank==0)  puts("======= Assemble MATIS matrix-finished    ========");

	Find_IS_Interface(M,&sum,&interface,&interfaceloc,&count_interface,&w,&interiorloc,&count_interior);
	if(rank==0)  puts("======= Finished with Mesh Manipulation   ========");
	Assemble_KE(sum,interface,count_interface,&K,rank,size,w);
	if(rank==0)  puts("=======              K Shell matrix       ========");
	Precon3(&P,M,count_interface,K,rank,size);

	if(rank==0)  puts("=======             G RHS VECTOR      ========");
	G_Vector(M,&g,f,interfaceloc,count_interface,interiorloc,count_interior);
	//VecView(f,PETSC_VIEWER_STDOUT_SELF);
	if(rank==0)  puts("=======              P Shell matrix       ========");
	Assemble_QLU(M,interface,interfaceloc,count_interface,Robin_par,&Q,&solver);
	if(rank==0)  puts("=======              Q Shell matrix       ========");
	if(n==0){
		QMINK(&QminK,K,Q,count_interface);
		RHSSymetric(&rhs,Q,g,count_interface);
		if(rank==0)  puts("=======         Symmetric 2LM Choosen       ========");
	}else{
		IminKQMINK(&QminK,K,Q,count_interface);
		RHSNONSymetric(&rhs,Q,K,g,count_interface);
		if(rank==0)  puts("=======        Non-Symmetric 2LM Choosen     ========");
	}

	//test_K(K,count_interface);

	if(rank==0)  puts("=======         Solve the 2LM Sytem    ========");
	//VecView(rhs,PETSC_VIEWER_STDOUT_WORLD);
	Solver_KSP(QminK,P,count_interface,rank,precon,rhs,&lambda);

	//if(rank==0)  puts("=======         Recover the Solution    ========");

	if(rank==size-1) RecoverSulution(solver,f,lambda,&solution,interfaceloc,count_interface);

	MatDestroy(&K);
	MatDestroy(&P);
	MatDestroy(&Q);
	MatDestroy(&QminK);
	VecDestroy(&rhs);

	PetscFinalize();
	return 0;
}
